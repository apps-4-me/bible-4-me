
#include "BibleDefs.h"

AbstractBibleModeldata::AbstractBibleModeldata (const int position, QObject * parent)
    : QObject    { parent }
    , m_position { position }
{ }

BibleVerse::BibleVerse (const int position, const QString & verseId, const QString & textContent, const bool marked, QObject * parent)
    : AbstractBibleModeldata { position, parent }
    , m_verseId              { verseId }
    , m_textContent          { textContent }
    , m_marked               { marked }
{ }

BibleChapter::BibleChapter (const int position, const QString & chapterId, QObject * parent)
    : AbstractBibleModeldata { position, parent }
    , m_chapterId            { chapterId }
    , m_verseModel           { { }, &BibleVerse::get_verseId }
{ }

BibleBook::BibleBook (const int position, const QString & bookId, QObject * parent)
    : AbstractBibleModeldata { position, parent }
    , m_bookId               { bookId }
    , m_chaptersModel        { { }, &BibleChapter::get_chapterId }
{ }

BibleText::BibleText (const QString & textKey, const QString & bibleId, const QString & bibleTitle, const bool hasLocal, QObject * parent)
    : QObject      { parent }
    , m_textKey    { textKey }
    , m_bibleId    { bibleId }
    , m_bibleTitle { bibleTitle }
    , m_hasLocal   { hasLocal }
    , m_isLoading  { false }
    , m_percent    { 0 }
{ }

BibleLanguage::BibleLanguage (const QString & languageId, const QString & languageTitle, QObject * parent)
    : QObject         { parent }
    , m_languageId    { languageId }
    , m_languageTitle { languageTitle }
    , m_nbLocals      { 0 }
    , m_expanded      { false }
    , m_textsModel    { { }, &BibleText::get_textKey }
{ }

BibleGroup::BibleGroup (const QString & bookId, QObject * parent)
    : QObject        { parent }
    , m_bookId       { bookId }
    , m_expanded     { false }
    , m_entriesModel { { }, &BibleVerse::get_verseId }
{ }
