#pragma once

#include <QObject>

#include "QmlEnumHelpers.hpp"
#include "QmlPropertyHelpers.hpp"
#include "QmlObjectListModel.hpp"

QML_ENUM_CLASS (BibleScope,
                FULL_BIBLE = -1,
                CURRENT_BOOK,
                CURRENT_CHAPTER,
                )

QML_ENUM_CLASS (PageIds,
                VIEW = -1,
                VERSIONS,
                SELECTOR,
                FAVORITES,
                SEARCH,
                CONFIG,
                )

QML_ENUM_CLASS (ZoomLevel,
                MEDIUM = -1,
                LARGE,
                SMALL,
                XLARGE,
                XSMALL,
                )

static const struct {
    const QString LANGUAGE_ID    { QStringLiteral ("languageId") };
    const QString LANGUAGE_TITLE { QStringLiteral ("languageTitle") };
    const QString BIBLE_ID       { QStringLiteral ("bibleId") };
    const QString BIBLE_TITLE    { QStringLiteral ("bibleTitle") };
    const QString BOOK_ID        { QStringLiteral ("bookId") };
    const QString CHAPTER_ID     { QStringLiteral ("chapterId") };
    const QString VERSE_ID       { QStringLiteral ("verseId") };
    const QString MARKED         { QStringLiteral ("marked") };
    const QString HAS_LOCAL      { QStringLiteral ("hasLocal") };
    const QString TEXT_CONTENT   { QStringLiteral ("textContent") };
} JSON;

class AbstractBibleModeldata : public QObject {
    Q_OBJECT
    QML_CONSTANT_VAR_PROPERTY (position, int)

public:
    explicit AbstractBibleModeldata (const int position,
                                     QObject * parent = nullptr);
};

class BibleVerse : public AbstractBibleModeldata {
    Q_OBJECT
    QML_CONSTANT_CSTREF_PROPERTY (verseId,     QString)
    QML_CONSTANT_CSTREF_PROPERTY (textContent, QString)
    QML_READONLY_VAR_PROPERTY    (marked,      bool)

public:
    explicit BibleVerse (const int       position,
                         const QString & verseId,
                         const QString & textContent,
                         const bool      marked,
                         QObject       * parent = nullptr);
};

class BibleChapter : public AbstractBibleModeldata {
    Q_OBJECT
    QML_CONSTANT_CSTREF_PROPERTY (chapterId,  QString)
    QML_OBJ_LIST_MODEL_PROPERTY  (verseModel, BibleVerse)

public:
    explicit BibleChapter (const int       position,
                           const QString & chapterId,
                           QObject *       parent = nullptr);
};

class BibleBook : public AbstractBibleModeldata {
    Q_OBJECT
    QML_CONSTANT_CSTREF_PROPERTY (bookId,        QString)
    QML_OBJ_LIST_MODEL_PROPERTY  (chaptersModel, BibleChapter)

public:
    explicit BibleBook (const int       position,
                        const QString & bookId,
                        QObject       * parent = nullptr);
};

class BibleText : public QObject {
    Q_OBJECT
    QML_CONSTANT_CSTREF_PROPERTY (textKey,    QString)
    QML_CONSTANT_CSTREF_PROPERTY (bibleId,    QString)
    QML_CONSTANT_CSTREF_PROPERTY (bibleTitle, QString)
    QML_READONLY_VAR_PROPERTY    (hasLocal,   bool)
    QML_READONLY_VAR_PROPERTY    (isLoading,  bool)
    QML_READONLY_VAR_PROPERTY    (percent,    int)

public:
    explicit BibleText (const QString & textKey    = { },
                        const QString & bibleId    = { },
                        const QString & bibleTitle = { },
                        const bool      hasLocal   = false,
                        QObject       * parent     = nullptr);
};

class BibleLanguage : public QObject {
    Q_OBJECT
    QML_CONSTANT_CSTREF_PROPERTY (languageId,    QString)
    QML_CONSTANT_CSTREF_PROPERTY (languageTitle, QString)
    QML_READONLY_VAR_PROPERTY    (nbLocals,      int)
    QML_WRITABLE_VAR_PROPERTY    (expanded,      bool)
    QML_OBJ_LIST_MODEL_PROPERTY  (textsModel,    BibleText)

public:
    explicit BibleLanguage (const QString & languageId    = { },
                            const QString & languageTitle = { },
                            QObject       * parent        = nullptr);
};

class BibleGroup : public QObject {
    Q_OBJECT
    QML_CONSTANT_CSTREF_PROPERTY (bookId,       QString)
    QML_WRITABLE_VAR_PROPERTY    (expanded,     bool)
    QML_OBJ_LIST_MODEL_PROPERTY  (entriesModel, BibleVerse)

public:
    explicit BibleGroup (const QString & bookId = { },
                         QObject       * parent = nullptr);
};
