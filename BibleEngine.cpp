
#include "BibleEngine.h"

#include <QDir>
#include <QFile>
#include <QDebug>
#include <QTranslator>
#include <QCoreApplication>
#include <QXmlStreamReader>
#include <QNetworkReply>
#include <QStringBuilder>
#include <QStandardPaths>
#include <QRegularExpression>
#include <QElapsedTimer>
#include <QtMath>
#include <QQmlEngine>

static const QString EMPTY_STR     { };
static const QString SEPARATOR     { QS ("__") };
static const QString REPOS_BASEURL { QS ("https://gitlab.com/apps-4-me/bible-4-me-osis-texts/-/raw/master") };

static QString getBasePath (void) {
    static const QString ret {
        //#ifdef Q_OS_ANDROID
        //    (QStandardPaths::writableLocation (QStandardPaths::GenericDataLocation) % "/BibleMe")
        //        #else
        QStandardPaths::writableLocation (QStandardPaths::AppDataLocation)
                //
    };
    return ret;
}

static QString getPathForConfig (void) {
    return QS ("%1/BibleMe.ini").arg (getBasePath ());
}

static QString getPathForIndex (void) {
    return QS ("%1/index.xml").arg (getBasePath ());
}

static QString getPathForText (const QString & textKey) {
    return QS ("%1/%2.xml").arg (getBasePath (), textKey);
}

static QString canonize (const QString & str) {
    static QRegularExpression REGEXP_DEL_NON_ASCII { QS ("[^a-zA-Z\\s]") };
    return str.simplified ().normalized (QString::NormalizationForm_KD).remove (REGEXP_DEL_NON_ASCII).toLower ();
}

static inline int computePercent (const int value, const int total) {
    return (total > 0 ? int (100.0 * qreal (value) / qreal (total)) : 0);
}

Theme::Theme (QObject * parent)
    : QObject       { parent }
    , m_nightMode   { false }
    , m_zoomRatio   { 1.0 }
    , m_fontName    { QS ("sans-serif") }
    , m_bgPrimary   { Qt::magenta }
    , m_bgSecondary { Qt::magenta }
    , m_bgHighlight { Qt::magenta }
    , m_bgClickable { Qt::magenta }
    , m_bgGradient1 { Qt::magenta }
    , m_bgGradient2 { Qt::magenta }
    , m_bgDanger    { Qt::magenta }
    , m_bgSuccess   { Qt::magenta }
    , m_bgWarning   { Qt::magenta }
    , m_fgText      { Qt::magenta }
    , m_fgSoft      { Qt::magenta }
    , m_fgBorder    { Qt::magenta }
    , m_fgNormal    { Qt::magenta }
    , m_fgSelected  { Qt::magenta }
    , m_fgDanger    { Qt::magenta }
    , m_fgSuccess   { Qt::magenta }
    , m_fontXS      { -1 }
    , m_fontS       { -1 }
    , m_fontM       { -1 }
    , m_fontL       { -1 }
    , m_spaceXS     { -1 }
    , m_spaceS      { -1 }
    , m_spaceM      { -1 }
    , m_spaceL      { -1 }
    , m_lineNone    { 0 }
    , m_lineSoft    { 1 }
    , m_lineStrong  { 2 }
{
    QQmlEngine::setObjectOwnership (this, QQmlEngine::ObjectOwnership::CppOwnership);
    connect (this, &Theme::nightModeChanged, this, &Theme::doRecomputePalette);
    connect (this, &Theme::zoomRatioChanged, this, &Theme::doRecomputeDimensions);
    doRecomputePalette    ();
    doRecomputeDimensions ();
}

Theme::~Theme (void) { }

QColor Theme::contrasted (const QColor & other) const {
    return ((other.redF () + other.greenF () + other.blueF ()) / 3.0 > 0.5 ? Qt::black : Qt::white);
}

void Theme::doRecomputePalette (void) {
    set_bgPrimary   (m_nightMode ? QS ("#1a160a") : QS ("#FCFBF7"));
    set_bgSecondary (m_nightMode ? QS ("#2f2a22") : QS ("#f1f0e5"));
    set_bgHighlight (m_nightMode ? QS ("#635329") : QS ("#FFDD85"));
    set_bgClickable (m_nightMode ? QS ("#A17000") : QS ("#ECBD37"));
    set_bgDanger    (m_nightMode ? QS ("#780000") : QS ("#d40000"));
    set_bgSuccess   (m_nightMode ? QS ("#193c0a") : QS ("#5b993e"));
    set_bgWarning   (m_nightMode ? QS ("#976d00") : QS ("#f3b000"));
    set_fgText      (m_nightMode ? QS ("#fdecb6") : QS ("#381F00"));
    set_fgBorder    (m_nightMode ? QS ("#6a6637") : QS ("#c0bb86"));
    set_fgSelected  (m_nightMode ? QS ("#A17000") : QS ("#CD9823"));
    set_fgDanger    (m_nightMode ? QS ("#ff1d1d") : QS ("#b40000"));
    set_fgSuccess   (m_nightMode ? QS ("#4ab11e") : QS ("#2e710c"));
}

void Theme::doRecomputeDimensions (void) {
    set_fontL   (qRound (20 * m_zoomRatio));
    set_fontM   (qRound (16 * m_zoomRatio));
    set_fontS   (qRound (14 * m_zoomRatio));
    set_fontXS  (qRound (12 * m_zoomRatio));
    set_spaceL  (qRound (16 * m_zoomRatio));
    set_spaceM  (qRound (8  * m_zoomRatio));
    set_spaceS  (qRound (4  * m_zoomRatio));
    set_spaceXS (qRound (2  * m_zoomRatio));
}

BibleEngine::BibleEngine (QObject * parent)
    : Theme               { parent }
    , m_isRefreshing      { false }
    , m_isLoading         { false }
    , m_isSearching       { false }
    , m_showLocalOnly     { false }
    , m_swipeChapters     { true }
    , m_textFontSize      { 0 }
    , m_refreshPercent    { 0 }
    , m_searchPercent     { 0 }
    , m_resultsTotal      { 0 }
    , m_favoritesTotal    { 0 }
    , m_previewTextSizeXS { get_fontM () }
    , m_previewTextSizeS  { get_fontM () }
    , m_previewTextSizeM  { get_fontM () }
    , m_previewTextSizeL  { get_fontM () }
    , m_previewTextSizeXL { get_fontM () }
    , m_currentChapterNum { 0 }
    , m_currentPage       { PageIds::VIEW }
    , m_currentScope      { BibleScope::FULL_BIBLE }
    , m_currentLevel      { ZoomLevel::MEDIUM }
    , m_currentTextSize   { get_fontM () }
    , m_currentVerseId    { QS ("John.3.16") }
    , m_languagesModel    { { }, &BibleLanguage::get_languageId }
    , m_booksModel        { { }, &BibleBook::get_bookId }
    , m_resultsModel      { { }, &BibleGroup::get_bookId }
    , m_favoritesModel    { { }, &BibleGroup::get_bookId }
    , m_currentLang       { nullptr }
    , m_currentText       { nullptr }
    , m_currentBook       { nullptr }
    , m_currentChapter    { nullptr }
    , m_currentVerse      { nullptr }
    , m_selectorBook      { nullptr }
    , m_selectorChapter   { nullptr }
    , m_selectorVerse     { nullptr }
    , m_settings          { getPathForConfig (), QSettings::IniFormat }
    , m_translator        { new QTranslator { this } }
{
    QDir { }.mkpath (getBasePath ());
    qInfo () << "SETTINGS :" << m_settings.fileName ();
    m_showLocalOnly          = m_settings.value (QS ("showLocalOnly"),  m_showLocalOnly).toBool ();
    m_swipeChapters          = m_settings.value (QS ("swipeChapters"),  m_swipeChapters).toBool ();
    m_textFontSize           = m_settings.value (QS ("textFontSize"),   m_textFontSize).toInt ();
    m_currentTranslationCode = m_settings.value (QS ("locale"),         m_currentTranslationCode).toString ();
    m_currentTextKey         = m_settings.value (QS ("currentTextKey"), m_currentTextKey).toString ();
    m_currentVerseId         = m_settings.value (QS ("currPosId"),      m_currentVerseId).toString ();
    m_favorites              = m_settings.value (QS ("bookmarks"),      m_favorites).toStringList ();
    m_currentLevel           = ZoomLevel::fromString (m_settings.value (QS ("level")).toString ());
    connect (this, &BibleEngine::textFontSizeChanged, [this] (void) {
        m_settings.setValue (QS ("textFontSize"), get_textFontSize ());
    });
    connect (this, &BibleEngine::currentTextKeyChanged, [this] (void) {
        m_settings.setValue (QS ("currentTextKey"), get_currentTextKey ());
    });
    connect (this, &BibleEngine::currentVerseIdChanged, [this] (void) {
        m_settings.setValue (QS ("currPosId"), get_currentVerseId ());
    });
    connect (this, &BibleEngine::showLocalOnlyChanged, [this] (void) {
        m_settings.setValue (QS ("showLocalOnly"), get_showLocalOnly ());
    });
    connect (this, &BibleEngine::nightModeChanged, [this] (void) {
        m_settings.setValue (QS ("nightMode"), get_nightMode ());
    });
    connect (this, &BibleEngine::swipeChaptersChanged, [this] (void) {
        m_settings.setValue (QS ("swipeChapters"), get_swipeChapters ());
    });
    connect (this, &BibleEngine::currentTranslationCodeChanged, [this] (void) {
        m_settings.setValue (QS ("locale"), get_currentTranslationCode ());
    });
    connect (this, &BibleEngine::currentLevelChanged, [this] (void) {
        m_settings.setValue (QS ("level"), ZoomLevel::asString (m_currentLevel));
    });
    connect (this, &BibleEngine::fontMChanged,        this, &BibleEngine::doRecomputeTextFontSizes);
    connect (this, &BibleEngine::currentLevelChanged, this, &BibleEngine::doRecomputeTextFontSizes);
    set_nightMode (m_settings.value (QS ("nightMode"), false).toBool ());
    if (m_currentTranslationCode.isEmpty ()) {
        m_currentTranslationCode = QLocale { }.name ().split ('_').first ().toLower ();
    }
    translateUi (m_currentTranslationCode);
    doLoadAndParseIndex ();
    loadText (m_currentTextKey);
    doRecomputeTextFontSizes ();
}

BibleEngine::~BibleEngine (void) { }

BibleEngine & BibleEngine::instance (void) {
    static BibleEngine ret { };
    return ret;
}

void BibleEngine::refreshIndex (void) {
    set_isRefreshing (true);
    set_refreshErrorMsg (QString { });
    QNetworkRequest request { QUrl { QS ("%1/index.xml").arg (REPOS_BASEURL) } };
    request.setRawHeader (QByteArrayLiteral ("Accept-Encoding"), QByteArrayLiteral ("identity"));
    if (QNetworkReply * reply { m_nam.get (request) }) {
        connect (reply, &QNetworkReply::downloadProgress, this, [this] (const qint64 bytesReceived, const qint64 bytesTotal) {
            set_refreshPercent (computePercent (bytesReceived, bytesTotal));
        });
        connect (reply, &QNetworkReply::finished, this, [this, reply] (void) {
            if (reply->error () == QNetworkReply::NoError) {
                QFile file { getPathForIndex () };
                if (file.open (QIODevice::WriteOnly | QIODevice::Truncate)) {
                    if (file.write (reply->readAll ()) <= 0) {
                        set_refreshErrorMsg (file.errorString ());
                    }
                    file.flush ();
                    file.close ();
                    doLoadAndParseIndex ();
                }
                else {
                    set_refreshErrorMsg (file.errorString ());
                }
            }
            else {
                set_refreshErrorMsg (reply->errorString ());
            }
            set_isRefreshing (false);
            reply->deleteLater ();
        });
    }
}

void BibleEngine::downloadText (const QString & languageId, const QString & bibleId) {
    if (BibleLanguage * bibleLanguage { m_languagesModel.getByUid (languageId) }) {
        const QString textKey { (languageId % SEPARATOR % bibleId) };
        if (BibleText * bibleText { bibleLanguage->textsModel ().getByUid (textKey) }) {
            bibleText->set_isLoading (true);
            QNetworkRequest request (QUrl { QS ("%1/%2/%3.xml").arg (REPOS_BASEURL, languageId, bibleId) });
            request.setRawHeader (QByteArrayLiteral ("Accept-Encoding"), QByteArrayLiteral ("identity"));
            if (QNetworkReply * reply { m_nam.get (request) }) {
                connect (reply, &QNetworkReply::downloadProgress, this, [bibleText] (const qint64 bytesReceived, const qint64 bytesTotal) {
                    bibleText->set_percent (computePercent (bytesReceived, bytesTotal));
                });
                connect (reply, &QNetworkReply::finished, this, [this, reply, bibleText] (void) {
                    if (reply->error () == QNetworkReply::NoError) {
                        QFile file { getPathForText (bibleText->get_textKey ()) };
                        if (file.open  (QIODevice::WriteOnly | QIODevice::Truncate)) {
                            if (file.write (reply->readAll ()) > 0) {
                                bibleText->set_hasLocal (true);
                                doUpdateLanguagesFlags ();
                            }
                            file.flush ();
                            file.close ();
                        }
                    }
                    bibleText->set_isLoading (false);
                    reply->deleteLater ();
                });
            }
        }
    }
}

void BibleEngine::loadText (const QString & textKey) {
    static QElapsedTimer benchmark { };
    if (!textKey.isEmpty ()) {
        benchmark.restart ();
        set_isLoading (true);
        set_currentSearch (QString { });
        set_currentTextKey (textKey);
        BibleLanguage * bibleLang { nullptr };
        BibleText * bibleText { nullptr };
        if ((bibleLang = m_languagesModel.getByUid (textKey.split (SEPARATOR).first ())) != nullptr) {
            if ((bibleText = bibleLang->textsModel ().getByUid (textKey)) != nullptr) { }
        }
        set_currentLang     (bibleLang);
        set_currentText     (bibleText);
        set_currentBook     (nullptr);
        set_currentChapter  (nullptr);
        set_currentVerse    (nullptr);
        set_selectorBook    (nullptr);
        set_selectorChapter (nullptr);
        set_selectorVerse   (nullptr);
        m_resultsModel.clear ();
        m_favoritesModel.clear ();
        int counter { 0 };
        int favoritesTotal { 0 };
        QFile file { getPathForText (textKey) };
        if (file.open (QIODevice::ReadOnly | QIODevice::Text)) {
            QXmlStreamReader xml (&file);
            static const QString OSIS_ID      { QS ("osisID") };
            static const QString TYPE_BOOK    { QS ("div") };
            static const QString TYPE_CHAPTER { QS ("chapter") };
            static const QString TYPE_VERSE   { QS ("verse") };
            while (!xml.atEnd () && !xml.hasError ()) {
                const QXmlStreamReader::TokenType token = xml.readNext ();
                const QStringRef type = xml.name ();
                if (type == TYPE_BOOK) {
                    if (token == QXmlStreamReader::StartElement) {
                        const QString bookId { xml.attributes ().value (OSIS_ID).toString () };
                        m_booksModel.append (new BibleBook {
                                                 ++counter,
                                                 bookId,
                                             });
                        m_favoritesModel.append (new BibleGroup {
                                                     bookId,
                                                 });
                        m_resultsModel.append (new BibleGroup {
                                                   bookId,
                                               });
                    }
                    else if (token == QXmlStreamReader::EndElement) { }
                }
                else if (type == TYPE_CHAPTER) {
                    if (token == QXmlStreamReader::StartElement) {
                        const QString chapterId { xml.attributes ().value (OSIS_ID).toString () };
                        m_booksModel.last ()->chaptersModel ().append (new BibleChapter {
                                                                           ++counter,
                                                                           chapterId,
                                                                       });
                    }
                    else if (token == QXmlStreamReader::EndElement) { }
                }
                else if (type == TYPE_VERSE) {
                    const QString verseId     { xml.attributes ().value (OSIS_ID).toString () };
                    const QString textContent { xml.readElementText (QXmlStreamReader::SkipChildElements) };
                    const bool    isMarked    { m_favorites.contains (verseId) };
                    m_booksModel.last ()->chaptersModel ().last ()->verseModel ().append (new BibleVerse {
                                                                                              ++counter,
                                                                                              verseId,
                                                                                              textContent,
                                                                                              isMarked,
                                                                                          });
                    if (isMarked) {
                        m_favoritesModel.last ()->entriesModel ().append (m_booksModel.last ()->chaptersModel ().last ()->verseModel ().last ());
                        ++favoritesTotal;
                    }
                }
                else { }
            }
            if (xml.hasError ()) {
                qWarning () << xml.errorString ();
            }
            xml.clear ();
            file.close ();
        }
        set_favoritesTotal (favoritesTotal);
        qWarning () << "Loaded" << textKey << "in" << benchmark.elapsed () << "ms";
        set_isLoading (false);
        selectPosition (m_currentVerseId);
    }
}

void BibleEngine::removeText (const QString & textKey) {
    const QString languageId { textKey.split (SEPARATOR).first () };
    if (BibleLanguage * bibleLanguage { m_languagesModel.getByUid (languageId) }) {
        if (BibleText * bibleText { bibleLanguage->textsModel ().getByUid (textKey) }) {
            if (QFile::remove (getPathForText (textKey))) {
                bibleText->set_hasLocal (false);
                doUpdateLanguagesFlags ();
            }
        }
    }
}

void BibleEngine::addBookmark (const QString & verseId) {
    if (!verseId.isEmpty () && !m_favorites.contains (verseId)) {
        selectPosition (verseId);
        if (m_selectorVerse != nullptr) {
            m_selectorVerse->set_marked (true);
            m_favorites.append (verseId);
            m_settings.setValue (QS ("bookmarks"), m_favorites);
            if (BibleGroup * bibleGroup { m_favoritesModel.getByUid (m_selectorBookId) }) {
                bibleGroup->entriesModel ().append (m_selectorVerse);
            }
        }
    }
}

void BibleEngine::removeBookmark (const QString & verseId) {
    if (!verseId.isEmpty () && m_favorites.contains (verseId)) {
        selectPosition (verseId);
        if (m_selectorVerse != nullptr) {
            m_selectorVerse->set_marked (false);
            m_favorites.removeAll (verseId);
            m_settings.setValue (QS ("bookmarks"), m_favorites);
            if (BibleGroup * bibleGroup { m_favoritesModel.getByUid (m_selectorBookId) }) {
                bibleGroup->entriesModel ().remove (m_selectorVerse);
            }
        }
    }
}

int BibleEngine::implLookupVerse (BibleBook * book, BibleVerse * verse, const QString & token, const int posStart, const int posEnd) {
    int ret { 0 };
    if (canonize (verse->get_textContent ()).contains (token)) {
        if (BibleGroup * bibleGroup { m_resultsModel.getByUid (book->get_bookId ()) }) {
            bibleGroup->entriesModel ().append (verse);
            ++ret;
        }
    }
    set_searchPercent (computePercent ((verse->get_position () - posStart), (posEnd - posStart)));
    return ret;
}

void BibleEngine::searchContent (const QString & filter) {
    static QElapsedTimer benchmark { };
    const QString canonicalToken = canonize (filter);
    if (canonicalToken.count () >= 3) {
        set_currentSearch (filter.simplified ());
        set_isSearching (true);
        benchmark.restart ();
        set_resultsTotal (0);
        for (BibleGroup * bibleGroup : m_resultsModel) {
            bibleGroup->set_expanded (false);
            bibleGroup->entriesModel ().clear ();
        }
        int resultCount   { 0 };
        int firstPosition { -1 };
        int lastPosition  { -1 };
        switch (m_currentScope) {
            case BibleScope::FULL_BIBLE: {
                firstPosition = m_booksModel.first ()->chaptersModel ().first ()->verseModel ().first ()->get_position ();
                lastPosition  = m_booksModel.last ()->chaptersModel ().last ()->verseModel ().last ()->get_position ();
                for (BibleBook * bibleBook : m_booksModel) {
                    for (BibleChapter * bibleChapter : bibleBook->chaptersModel ()) {
                        for (BibleVerse * bibleVerse : bibleChapter->verseModel ()) {
                            resultCount += implLookupVerse (bibleBook, bibleVerse, canonicalToken, firstPosition, lastPosition);
                        }
                    }
                }
                break;
            }
            case BibleScope::CURRENT_BOOK: {
                if (m_currentBook != nullptr) {
                    firstPosition = m_currentBook->chaptersModel ().first ()->verseModel ().first ()->get_position ();
                    lastPosition  = m_currentBook->chaptersModel ().last ()->verseModel ().last ()->get_position ();
                    for (BibleChapter * bibleChapter : m_currentBook->chaptersModel ()) {
                        for (BibleVerse * bibleVerse : bibleChapter->verseModel ()) {
                            resultCount += implLookupVerse (m_currentBook, bibleVerse, canonicalToken, firstPosition, lastPosition);
                        }
                    }
                }
                break;
            }
            case BibleScope::CURRENT_CHAPTER: {
                if (m_currentChapter != nullptr) {
                    firstPosition = m_currentChapter->verseModel ().first ()->get_position ();
                    lastPosition  = m_currentChapter->verseModel ().last ()->get_position ();
                    for (BibleVerse * bibleVerse : m_currentChapter->verseModel ()) {
                        resultCount += implLookupVerse (m_currentBook, bibleVerse, canonicalToken, firstPosition, lastPosition);
                    }
                }
                break;
            }
        }
        qWarning () << "Searched" << filter << "in" << benchmark.elapsed () << "ms";
        set_resultsTotal (resultCount);
        set_isSearching (false);
    }
}

void BibleEngine::translateUi (const QString & code) {
    QCoreApplication::removeTranslator (m_translator);
    QString locale { code };
    const QString qm { QS (":/lang/%1.qm").arg (locale) };
    if (QFile::exists (qm)) {
        m_translator->load (qm);
    }
    else {
        m_translator->load (QS (":/lang/en.qm"));
        locale = QS ("en");
    }
    if (m_translator->isEmpty ()) {
        qWarning () << "Translation is empty for language" << code;
    }
    QCoreApplication::installTranslator (m_translator);
    m_booksFullName.clear ();
    set_currentTranslationCode (locale);
}

void BibleEngine::selectPosition (const QString & refId) {
    const QStringList parts      { refId.split ('.') };
    const QString     bookAbbr   { (parts.count () >= 1 ? parts.at (0) : EMPTY_STR) };
    const QString     chapterNum { (parts.count () >= 2 ? parts.at (1) : EMPTY_STR) };
    const QString     verseNum   { (parts.count () >= 3 ? parts.at (2) : EMPTY_STR) };
    set_selectorBookId    (bookAbbr);
    set_selectorChapterId (!chapterNum.isEmpty () ? bookAbbr % '.' % chapterNum : EMPTY_STR);
    set_selectorVerseId   (!verseNum.isEmpty () ? bookAbbr % '.' % chapterNum % '.' % verseNum : EMPTY_STR);
    BibleBook    * bibleBook    { nullptr };
    BibleChapter * bibleChapter { nullptr };
    BibleVerse   * bibleVerse   { nullptr };
    if ((bibleBook = m_booksModel.getByUid (m_selectorBookId)) != nullptr) {
        if ((bibleChapter = bibleBook->chaptersModel ().getByUid (m_selectorChapterId)) != nullptr) {
            if ((bibleVerse = bibleChapter->verseModel ().getByUid (m_selectorVerseId)) != nullptr) { }
        }
    }
    set_selectorBook    (bibleBook);
    set_selectorChapter (bibleChapter);
    set_selectorVerse   (bibleVerse);
    if (bibleBook != nullptr && bibleChapter != nullptr && bibleVerse != nullptr) {
        set_currentBookId     (m_selectorBookId);
        set_currentChapterId  (m_selectorChapterId);
        set_currentVerseId    (m_selectorVerseId);
        set_currentBook       (bibleBook);
        set_currentChapter    (bibleChapter);
        set_currentVerse      (bibleVerse);
        set_currentChapterNum (bibleBook->chaptersModel ().indexOf (bibleChapter) +1);
        qInfo () << "VIEW :" << m_currentBookId << m_currentChapterId << m_currentVerseId << m_currentChapterNum;
    }
    else {
        qInfo () << "SELECTOR :" << m_selectorBookId << m_selectorChapterId << m_selectorVerseId;
    }
}

QString BibleEngine::formatReference (const QString & refId, const bool onlyLast) {
    QString ret { };
    if (m_booksFullName.isEmpty ()) {
        m_booksFullName = QHash<QString, QString> {
            { "1Chr",    tr ("1 Chronicles")         },
            { "1Cor",    tr ("1 Corinthians")        },
            { "1Esd",    tr ("1 Ezra")               },
            { "1John",   tr ("1 John")               },
            { "1Kgs",    tr ("1 Kings")              },
            { "1Macc",   tr ("1 Maccabees")          },
            { "1Pet",    tr ("1 Peter")              },
            { "1Sam",    tr ("1 Samuel")             },
            { "1Thess",  tr ("1 Thessalonians")      },
            { "1Tim",    tr ("1 Timothy")            },
            { "2Chr",    tr ("2 Chronicles")         },
            { "2Cor",    tr ("2 Corinthians")        },
            { "2Esd",    tr ("2 Ezra")               },
            { "2John",   tr ("2 John")               },
            { "2Kgs",    tr ("2 Kings")              },
            { "2Macc",   tr ("2 Maccabees")          },
            { "2Pet",    tr ("2 Peter")              },
            { "2Sam",    tr ("2 Samuel")             },
            { "2Thess",  tr ("2 Thessalonians")      },
            { "2Tim",    tr ("2 Timothy")            },
            { "3John",   tr ("3 John")               },
            { "Acts",    tr ("Acts")                 },
            { "Amos",    tr ("Amos")                 },
            { "Bar",     tr ("Baruch")               },
            { "Col",     tr ("Colossians")           },
            { "Dan",     tr ("Daniel")               },
            { "Deut",    tr ("Deuteronomy")          },
            { "Eccl",    tr ("Ecclesiastes")         },
            { "Eph",     tr ("Ephesians")            },
            { "Esth",    tr ("Esther")               },
            { "Exod",    tr ("Exodus")               },
            { "Ezek",    tr ("Ezekiel")              },
            { "Ezra",    tr ("Ezra")                 },
            { "Gal",     tr ("Galatians")            },
            { "Gen",     tr ("Genesis")              },
            { "Hab",     tr ("Habakkuk")             },
            { "Hag",     tr ("Haggai")               },
            { "Heb",     tr ("Hebrews")              },
            { "Hos",     tr ("Hosea")                },
            { "Isa",     tr ("Isaiah")               },
            { "Jas",     tr ("James")                },
            { "Jdt",     tr ("Judith")               },
            { "Jer",     tr ("Jeremiah")             },
            { "Job",     tr ("Job")                  },
            { "Joel",    tr ("Joel")                 },
            { "John",    tr ("John")                 },
            { "Jonah",   tr ("Jonah")                },
            { "Josh",    tr ("Joshua")               },
            { "Jude",    tr ("Jude")                 },
            { "Judg",    tr ("Judges")               },
            { "Lam",     tr ("Lamentations")         },
            { "Lao",     tr ("Laodiceans")           },
            { "Lev",     tr ("Leviticus")            },
            { "Luke",    tr ("Luke")                 },
            { "Mal",     tr ("Malachi")              },
            { "Mark",    tr ("Mark")                 },
            { "Matt",    tr ("Matthew")              },
            { "Mic",     tr ("Micah")                },
            { "Nah",     tr ("Nahum")                },
            { "Neh",     tr ("Nehemiah")             },
            { "Num",     tr ("Numbers")              },
            { "Obad",    tr ("Obadiah")              },
            { "Phil",    tr ("Philippians")          },
            { "Phlm",    tr ("Philemon")             },
            { "PrMan",   tr ("Prayer of Manasseh")   },
            { "Prov",    tr ("Proverbs")             },
            { "Ps",      tr ("Psalms")               },
            { "Rev",     tr ("Revelation")           },
            { "Rom",     tr ("Romans")               },
            { "Ruth",    tr ("Ruth")                 },
            { "Sir",     tr ("Sirach")               },
            { "Song",    tr ("Song of Solomon")      },
            { "Titus",   tr ("Titus")                },
            { "Tob",     tr ("Tobit")                },
            { "Wis",     tr ("Wisdom")               },
            { "Zech",    tr ("Zechariah")            },
            { "Zeph",    tr ("Zephaniah")            },
        };
    }
    const QStringList tmp        { refId.split ('.') };
    const QString     bookAbbr   { (tmp.count () >= 1 ? tmp.at (0) : EMPTY_STR) };
    const QString     chapterNum { (tmp.count () >= 2 ? tmp.at (1) : EMPTY_STR) };
    const QString     verseNum   { (tmp.count () >= 3 ? tmp.at (2) : EMPTY_STR) };
    if (onlyLast) {
        if (!verseNum.isEmpty ()) {
            ret = tr ("v. %1").arg (verseNum);
        }
        else if (!chapterNum.isEmpty ()) {
            ret = tr ("chap. %1").arg (chapterNum);
        }
        else if (!bookAbbr.isEmpty ()) {
            ret = m_booksFullName.value (bookAbbr);
        }
    }
    else {
        if (!bookAbbr.isEmpty ()) {
            ret += m_booksFullName.value (bookAbbr);
            if (!chapterNum.isEmpty ()) {
                ret += tr (", chap. %1").arg (chapterNum);
                if (!verseNum.isEmpty ()) {
                    ret += tr (", v. %1").arg (verseNum);
                }
            }
        }
    }
    if (ret.isEmpty () && !refId.isEmpty ()) {
        ret = ("!!" % refId);
        qWarning () << "Unknown book" << bookAbbr;
    }
    return ret;
}

void BibleEngine::doUpdateLanguagesFlags (void) {
    for (BibleLanguage * bibleLanguage : m_languagesModel) {
        int nbLocal { 0 };
        for (BibleText * bibleText : bibleLanguage->textsModel ()) {
            if (bibleText->get_hasLocal ()) {
                ++nbLocal;
            }
        }
        bibleLanguage->set_nbLocals (nbLocal);
    }
}

void BibleEngine::doLoadAndParseIndex (void) {
    static const QString XML_BIBLES   { QS ("bibles") };
    static const QString XML_BIBLE    { QS ("bible") };
    static const QString XML_LANGUAGE { QS ("language") };
    static const QString XML_ID       { QS ("id") };
    static const QString XML_TITLE    { QS ("title") };
    m_languagesModel.clear ();
    QFile file { getPathForIndex () };
    if (file.open (QIODevice::ReadOnly | QIODevice::Text)) {
        QXmlStreamReader xml { &file };
        while (!xml.atEnd () && !xml.hasError ()) {
            if (xml.readNext () == QXmlStreamReader::StartElement) {
                if (xml.name () == XML_BIBLES) {
                    // NOTE : nothing special, just root node
                }
                else if (xml.name () == XML_LANGUAGE) {
                    const QString langId   { xml.attributes ().value (XML_ID).toString () };
                    const QString langName { xml.attributes ().value (XML_TITLE).toString () };
                    m_languagesModel.append (new BibleLanguage {
                                                 langId,
                                                 langName,
                                             });
                }
                else if (xml.name () == XML_BIBLE) {
                    const QString bibleId   { xml.attributes ().value (XML_ID).toString () };
                    const QString bibleName { xml.attributes ().value (XML_TITLE).toString () };
                    const QString textKey   { (m_languagesModel.last ()->get_languageId () % SEPARATOR % bibleId) };
                    const bool    isLocal   { QFile::exists (getPathForText (textKey)) };
                    m_languagesModel.last ()->textsModel ().append (new BibleText {
                                                                        textKey,
                                                                        bibleId,
                                                                        bibleName,
                                                                        isLocal,
                                                                    });
                }
            }
        }
        if (xml.hasError ()) {
            qWarning () << xml.errorString ();
        }
        file.close ();
    }
    doUpdateLanguagesFlags ();
}

void BibleEngine::doRecomputeTextFontSizes (void) {
    set_previewTextSizeXS (qRound (get_fontM () * 0.65));
    set_previewTextSizeS  (qRound (get_fontM () * 0.85));
    set_previewTextSizeM  (qRound (get_fontM () * 1.00));
    set_previewTextSizeL  (qRound (get_fontM () * 1.15));
    set_previewTextSizeXL (qRound (get_fontM () * 1.35));
    set_currentTextSize   ([this] (void) -> int {
        switch (m_currentLevel) {
            case ZoomLevel::LARGE:  return m_previewTextSizeL;
            case ZoomLevel::SMALL:  return m_previewTextSizeS;
            case ZoomLevel::XLARGE: return m_previewTextSizeXL;
            case ZoomLevel::XSMALL: return m_previewTextSizeXS;
            default:                break;
        }
        return m_previewTextSizeM;
    } ());
}
