#pragma once

#include "BibleDefs.h"

#include <QNetworkAccessManager>
#include <QSettings>
#include <QColor>
#include <QTranslator>

#define QS QStringLiteral

class Theme : public QObject {
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY    (nightMode,   bool)
    QML_WRITABLE_VAR_PROPERTY    (zoomRatio,   qreal)
    QML_WRITABLE_CSTREF_PROPERTY (fontName,    QString)
    /// backgrounds :
    QML_READONLY_CSTREF_PROPERTY (bgPrimary,   QColor)
    QML_READONLY_CSTREF_PROPERTY (bgSecondary, QColor)
    QML_READONLY_CSTREF_PROPERTY (bgHighlight, QColor)
    QML_READONLY_CSTREF_PROPERTY (bgClickable, QColor)
    QML_READONLY_CSTREF_PROPERTY (bgGradient1, QColor)
    QML_READONLY_CSTREF_PROPERTY (bgGradient2, QColor)
    QML_READONLY_CSTREF_PROPERTY (bgDanger,    QColor)
    QML_READONLY_CSTREF_PROPERTY (bgSuccess,   QColor)
    QML_READONLY_CSTREF_PROPERTY (bgWarning,   QColor)
    /// foregrounds :
    QML_READONLY_CSTREF_PROPERTY (fgText,      QColor)
    QML_READONLY_CSTREF_PROPERTY (fgSoft,      QColor)
    QML_READONLY_CSTREF_PROPERTY (fgBorder,    QColor)
    QML_READONLY_CSTREF_PROPERTY (fgNormal,    QColor)
    QML_READONLY_CSTREF_PROPERTY (fgSelected,  QColor)
    QML_READONLY_CSTREF_PROPERTY (fgDanger,    QColor)
    QML_READONLY_CSTREF_PROPERTY (fgSuccess,   QColor)
    /// font sizes :
    QML_READONLY_VAR_PROPERTY    (fontXS,      int)
    QML_READONLY_VAR_PROPERTY    (fontS,       int)
    QML_READONLY_VAR_PROPERTY    (fontM,       int)
    QML_READONLY_VAR_PROPERTY    (fontL,       int)
    /// space sizes :
    QML_READONLY_VAR_PROPERTY    (spaceXS,     int)
    QML_READONLY_VAR_PROPERTY    (spaceS,      int)
    QML_READONLY_VAR_PROPERTY    (spaceM,      int)
    QML_READONLY_VAR_PROPERTY    (spaceL,      int)
    /// line sizes :
    QML_CONSTANT_VAR_PROPERTY    (lineNone,    int)
    QML_CONSTANT_VAR_PROPERTY    (lineSoft,    int)
    QML_CONSTANT_VAR_PROPERTY    (lineStrong,  int)

public:
    explicit Theme (QObject * parent = nullptr);
    virtual ~Theme (void);

    Q_INVOKABLE QColor contrasted (const QColor & other) const;

protected slots:
    void doRecomputePalette    (void);
    void doRecomputeDimensions (void);
};

class BibleEngine : public Theme {
    Q_OBJECT
    QML_READONLY_VAR_PROPERTY    (isRefreshing,           bool)
    QML_READONLY_VAR_PROPERTY    (isLoading,              bool)
    QML_READONLY_VAR_PROPERTY    (isSearching,            bool)
    QML_WRITABLE_VAR_PROPERTY    (showLocalOnly,          bool)
    QML_WRITABLE_VAR_PROPERTY    (swipeChapters,          bool)
    QML_WRITABLE_VAR_PROPERTY    (textFontSize,           int)
    QML_READONLY_VAR_PROPERTY    (refreshPercent,         int)
    QML_READONLY_VAR_PROPERTY    (searchPercent,          int)
    QML_READONLY_VAR_PROPERTY    (resultsTotal,           int)
    QML_READONLY_VAR_PROPERTY    (favoritesTotal,         int)
    QML_READONLY_VAR_PROPERTY    (previewTextSizeXS,      int)
    QML_READONLY_VAR_PROPERTY    (previewTextSizeS,       int)
    QML_READONLY_VAR_PROPERTY    (previewTextSizeM,       int)
    QML_READONLY_VAR_PROPERTY    (previewTextSizeL,       int)
    QML_READONLY_VAR_PROPERTY    (previewTextSizeXL,      int)
    QML_READONLY_VAR_PROPERTY    (currentChapterNum,      int)
    QML_WRITABLE_VAR_PROPERTY    (currentPage,            PageIds::Type)
    QML_WRITABLE_VAR_PROPERTY    (currentScope,           BibleScope::Type)
    QML_WRITABLE_VAR_PROPERTY    (currentLevel,           ZoomLevel::Type)
    QML_READONLY_VAR_PROPERTY    (currentTextSize,        int)
    QML_READONLY_CSTREF_PROPERTY (refreshErrorMsg,        QString)
    QML_READONLY_CSTREF_PROPERTY (currentSearch,          QString)
    QML_READONLY_CSTREF_PROPERTY (currentTranslationCode, QString)
    QML_READONLY_CSTREF_PROPERTY (currentTextKey,         QString)
    QML_READONLY_CSTREF_PROPERTY (currentBookId,          QString)
    QML_READONLY_CSTREF_PROPERTY (currentChapterId,       QString)
    QML_READONLY_CSTREF_PROPERTY (currentVerseId,         QString)
    QML_READONLY_CSTREF_PROPERTY (selectorBookId,         QString)
    QML_READONLY_CSTREF_PROPERTY (selectorChapterId,      QString)
    QML_READONLY_CSTREF_PROPERTY (selectorVerseId,        QString)
    QML_OBJ_LIST_MODEL_PROPERTY  (languagesModel,         BibleLanguage)
    QML_OBJ_LIST_MODEL_PROPERTY  (booksModel,             BibleBook)
    QML_OBJ_LIST_MODEL_PROPERTY  (resultsModel,           BibleGroup)
    QML_OBJ_LIST_MODEL_PROPERTY  (favoritesModel,         BibleGroup)
    QML_READONLY_PTR_PROPERTY    (currentLang,            BibleLanguage)
    QML_READONLY_PTR_PROPERTY    (currentText,            BibleText)
    QML_READONLY_PTR_PROPERTY    (currentBook,            BibleBook)
    QML_READONLY_PTR_PROPERTY    (currentChapter,         BibleChapter)
    QML_READONLY_PTR_PROPERTY    (currentVerse,           BibleVerse)
    QML_READONLY_PTR_PROPERTY    (selectorBook,           BibleBook)
    QML_READONLY_PTR_PROPERTY    (selectorChapter,        BibleChapter)
    QML_READONLY_PTR_PROPERTY    (selectorVerse,          BibleVerse)

public:
    explicit BibleEngine (QObject * parent = nullptr);
    virtual ~BibleEngine (void);

    static BibleEngine & instance (void);

    Q_INVOKABLE void refreshIndex   (void);
    Q_INVOKABLE void downloadText   (const QString & languageId, const QString & bibleId);
    Q_INVOKABLE void loadText       (const QString & textKey);
    Q_INVOKABLE void removeText     (const QString & textKey);
    Q_INVOKABLE void addBookmark    (const QString & verseId);
    Q_INVOKABLE void removeBookmark (const QString & verseId);
    Q_INVOKABLE void searchContent  (const QString & filter);
    Q_INVOKABLE void translateUi    (const QString & code);
    Q_INVOKABLE void selectPosition (const QString & refId);

    Q_INVOKABLE QString formatReference (const QString & refId, const bool onlyLast = false);

protected:
    void doUpdateLanguagesFlags   (void);
    void doLoadAndParseIndex      (void);
    void doRecomputeTextFontSizes (void);

    int implLookupVerse (BibleBook * book, BibleVerse * verse, const QString & token, const int posStart, const int posEnd);

private:
    QSettings     m_settings;
    QStringList   m_favorites;
    QNetworkAccessManager m_nam;
    QTranslator * m_translator;
    QHash<QString, QString> m_booksFullName;
};
