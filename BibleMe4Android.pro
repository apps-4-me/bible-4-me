
TEMPLATE = app

TARGET = BibleMe

QT += core network qml gui quick svg

CONFIG += c++11
CONFIG -= qtquickcompiler

INCLUDEPATH += \
    $$PWD \
    $$PWD/libQtQmlTricks

SOURCES += \
    $$PWD/main.cpp \
    $$PWD/BibleDefs.cpp \
    $$PWD/BibleEngine.cpp \
    $$PWD/libQtQmlTricks/QQuickAbstractContainerBase.cpp \
    $$PWD/libQtQmlTricks/QQuickColumnContainer.cpp \
    $$PWD/libQtQmlTricks/QQuickContainerAttachedObject.cpp \
    $$PWD/libQtQmlTricks/QQuickExtraAnchors.cpp \
    $$PWD/libQtQmlTricks/QQuickRowContainer.cpp

HEADERS += \
    $$PWD/BibleDefs.h \
    $$PWD/BibleEngine.h \
    $$PWD/libQtQmlTricks/QmlEnumHelpers.hpp \
    $$PWD/libQtQmlTricks/QmlListPropertyHelper.hpp \
    $$PWD/libQtQmlTricks/QmlObjectListModel.hpp \
    $$PWD/libQtQmlTricks/QmlPropertyHelpers.hpp \
    $$PWD/libQtQmlTricks/QQmlContainerEnums.h \
    $$PWD/libQtQmlTricks/QQuickAbstractContainerBase.h \
    $$PWD/libQtQmlTricks/QQuickColumnContainer.h \
    $$PWD/libQtQmlTricks/QQuickContainerAttachedObject.h \
    $$PWD/libQtQmlTricks/QQuickExtraAnchors.h \
    $$PWD/libQtQmlTricks/QQuickRowContainer.h

RESOURCES += \
    $$PWD/data.qrc

TRANSLATIONS += \
    $$PWD/lang/en.ts \
    $$PWD/lang/fr.ts

android {

DISTFILES += \
    $$PWD/android/AndroidManifest.xml \
    $$PWD/android/gradle/wrapper/gradle-wrapper.jar \
    $$PWD/android/gradlew \
    $$PWD/android/res/values/libs.xml \
    $$PWD/android/build.gradle \
    $$PWD/android/gradle/wrapper/gradle-wrapper.properties \
    $$PWD/android/gradlew.bat \
    $$PWD/android/res/xml/network_security_config.xml

}

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
ANDROID_TARGET_SDK_VERSION = 29
