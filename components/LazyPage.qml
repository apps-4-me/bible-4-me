import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;

FocusScope {
    id: _self;
    enabled: _self.needed;
    opacity: (_self.needed || Engine.currentPage === PageIds.VIEW ? 1 : 0);
    anchors.fill: parent;
    onNeededChanged: {
        if (_self.needed) {
            _loader.active = true;
        }
    }

    property bool needed : false;

    default property alias content : _loader.sourceComponent;

    signal contextActionsRequested (BibleVerse verse);

    Loader {
        id: _loader;
        active: false;
        anchors.fill: parent;
    }
}
