import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;

Rectangle {
    color: Engine.fgBorder;
    implicitWidth: Engine.lineSoft;
    implicitHeight: Engine.lineSoft;
}
