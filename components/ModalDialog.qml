import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;
import "../components";

MouseArea {
    id: _self;
    anchors.fill: parent;
    onPressed: { }
    onReleased: { }

    default property alias content: _layout.data;

    function hide () {
        _self.destroy ();
    }

    Rectangle {
        color: Engine.bgSecondary;
        opacity: 0.85;
        anchors.fill: parent;
    }
    Item {
        anchors {
            fill: parent;
            bottomMargin: ((Qt.inputMethod.visible && Qt.inputMethod.keyboardRectangle.y < Window.window.height)
                           ? (Window.window.height - Qt.inputMethod.keyboardRectangle.y)
                           : 0);
        }

        Rectangle {
            id: _frame;
            color: Engine.bgSecondary;
            radius: Engine.spaceS;
            implicitHeight: (_layout.implicitHeight + _layout.anchors.margins * 2);
            border {
                width: Engine.lineStrong;
                color: Engine.fgBorder;
            }
            anchors {
                margins: (Engine.spaceL * 2);
                verticalCenter: parent.verticalCenter;
            }
            ExtraAnchors.horizontalFill: parent;

            ColumnContainer {
                id: _layout;
                spacing: Engine.spaceL;
                anchors.margins: Engine.spaceL;
                ExtraAnchors.topDock: parent;
            }
        }
    }
}
