import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;

Item {
    id: _self;
    implicitWidth: _clicker.implicitWidth;
    implicitHeight: _clicker.implicitHeight;

    property bool value: false;

    signal edited (bool newValue);

    MouseArea {
        id: _clicker;
        implicitWidth: (Engine.spaceM * 6);
        implicitHeight: (Engine.spaceM * 3);
        onClicked: {
            _self.value = !_self.value;
            _self.edited (_self.value);
        }

        Rectangle {
            id: _back;
            color: (_self.value ? Engine.bgHighlight : Engine.bgSecondary);
            radius: (_back.height * 0.5);
            anchors.fill: parent;
        }
        Item {
            id: _groove;
            anchors {
                fill: parent;
                margins: (Engine.spaceS / 2);
            }

            Rectangle {
                id: _handle;
                x: (_self.value ? _groove.width - _handle.width : 0);
                color: Engine.bgClickable;
                radius: (_handle.height * 0.5);
                implicitWidth: _handle.height;
                ExtraAnchors.verticalFill: parent;

                Behavior on x {
                    NumberAnimation {
                        duration: 150;
                    }
                }
            }
        }
    }
}
