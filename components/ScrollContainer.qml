import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;

FocusScope {
    id: _self;
    clip: true;

    property int padding: 0;
    property alias spacing: _container.spacing;
    property alias contentY: _flicker.contentY;
    property alias contentHeight: _flicker.contentHeight;

    default property alias content: _container.data;

    property var headerItem;
    property var footerItem;
    property var indicatorOnly;
    property var placeholder;

    function ensureVisible (item) {
       //_flicker.ensureVisible (item);
    }

    Flickable {
        id: _flicker;
        contentWidth: _flicker.width;
        contentHeight: (_container.implicitHeight + _container.anchors.margins * 2);
        //boundsBehavior: Flickable.StopAtBounds;
        anchors.right: _scrollbar.left;
        ExtraAnchors.leftDock: parent;

        ColumnContainer {
            id: _container;
            anchors.margins: _self.padding;
            ExtraAnchors.topDock: parent;
        }
    }
    Scrollbar {
        id: _scrollbar;
        flickable: _flicker;
        ExtraAnchors.rightDock: parent;
        Container.ignored: true;
    }
}
