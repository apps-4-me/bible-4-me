import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;

FocusScope {
    id: _self;
    clip: true;

    property alias model: _list.model;
    property alias delegate: _list.delegate;
    property alias placeholder: _lbl.text;

    readonly property alias count: _list.count;

    ListView {
        id: _list;
        boundsBehavior: Flickable.StopAtBounds;
        anchors.right: _scrollbar.left;
        ExtraAnchors.leftDock: parent;
    }
    Scrollbar {
        id: _scrollbar;
        flickable: _list;
        ExtraAnchors.rightDock: parent;
    }
    TextLabel {
        id: _lbl;
        color: Engine.fgNormal;
        visible: !_list.count;
        wrapMode: Text.Wrap;
        verticalAlignment: Text.AlignVCenter;
        horizontalAlignment: Text.AlignHCenter;
        anchors {
            fill: parent;
            margins: (Engine.spaceL * 2);
        }
    }
}
