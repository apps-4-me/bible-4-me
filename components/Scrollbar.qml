import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;

Item {
    id: _self;
    visible: (_self.flickable.contentHeight > _self.flickable.height);
    implicitWidth: Engine.spaceS;

    property Flickable flickable: null;

    Rectangle {
        color: Engine.bgSecondary;
        anchors.fill: parent;
    }
    Rectangle {
        id: _handle;
        y: ((_self.height - _handle.height) * _self.flickable.contentY / (_self.flickable.contentHeight - _self.flickable.height));
        color: Engine.fgSelected;
        visible: (_self.flickable.contentHeight > _self.flickable.height);
        opacity: 0.85;
        implicitHeight: Math.max ((64  * Engine.zoomRatio), _self.height * _self.flickable.height / _self.flickable.contentHeight);
        ExtraAnchors.horizontalFill: parent;
    }
}
