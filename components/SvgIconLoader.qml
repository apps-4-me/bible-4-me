import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;

Item {
    id: _self;
    implicitWidth: _self.size;
    implicitHeight: _self.size;

    property int size: (20 * Engine.zoomRatio);
    property color color: Engine.fgText;
    property string icon: "";

    Image {
        id: _img;
        cache: true;
        source: _self.icon;
        fillMode: Image.PreserveAspectFit;
        asynchronous: false;
        verticalAlignment: Image.AlignVCenter;
        horizontalAlignment: Image.AlignHCenter;
        sourceSize {
            width: _self.size;
            height: _self.size;
        }
        layer {
            enabled: true;
            effect: ShaderEffect {
                fragmentShader: "
                    varying highp vec2 qt_TexCoord0;
                    uniform highp float qt_Opacity;
                    uniform highp sampler2D source;
                    uniform highp vec4 color;
                    void main() {
                       gl_FragColor = (color * texture2D (source, qt_TexCoord0).a * qt_Opacity);
                    }
                ";

                readonly property color color: _self.color;
            }
        }
        anchors.centerIn: parent;
    }
}
