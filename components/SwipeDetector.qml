import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;

MouseArea {
    id: _self;
    ExtraAnchors.horizontalFill: parent;

    property bool selected: false;

    Rectangle {
        color: Engine.bgHighlight;
        opacity: (_self.selected ? 0.65 : (_self.containsPress ? 0.35 : 0));
        anchors {
            fill: parent;
            rightMargin: -Engine.spaceS;
        }

        Behavior on opacity {
            OpacityAnimator {
                duration: 150;
            }
        }
    }
}
