import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;

FocusScope {
    id: _self;
    opacity: (_self.enabled ? 1.0 : 0.35);
    visible: (_self.editable || _self.value);
    implicitWidth: (_self.editable ? Math.max (_input.implicitWidth, _lbl.implicitWidth, _metrics.width + _self.padding * 2) : _input.implicitWidth);
    implicitHeight: (_self.editable ? Math.max (_input.implicitHeight, _lbl.implicitHeight, _metrics.height + _self.padding * 2) : _input.implicitHeight);
    activeFocusOnTab: true;

    property alias value: _input.text;
    property alias padding: _input.padding;
    property alias echoMode: _input.echoMode;
    property alias validator: _input.validator;
    property alias placeholder: _lbl.text;
    property alias foreground: _lbl.color;
    property alias background: _back.color;
    property alias format: _metrics.text;
    property bool mandatory: false;
    property bool editable: true;

    // compat :
    property alias text: _input.text;

    readonly property bool isEmpty: (_self.value.trim () === "");

    signal edited (string newValue);

    function select () {
        _input.forceActiveFocus ();
        _input.selectAll ();
    }

    Rectangle {
        id: _back;
        color: Engine.bgPrimary;
        visible: (_self.enabled && _self.editable);
        anchors.fill: parent;
    }
    Rectangle {
        id: _line;
        color: ((_self.mandatory && _self.value === "")
                ? Engine.fgDanger
                : (_self.activeFocus
                   ? Engine.fgSelected
                   : Engine.fgBorder));
        visible: _self.editable;
        implicitHeight: Engine.lineStrong;
        ExtraAnchors.bottomDock: parent;
    }
    Text {
        id: _lbl;
        font: _input.font;
        color: Engine.fgBorder;
        padding: _self.padding;
        visible: (_self.value === "" && _self.editable && !_input.activeFocus);
        ExtraAnchors.horizontalFill: parent;
    }
    TextInput {
        id: _input;
        clip: true;
        focus: true;
        color: Engine.fgText;
        padding: Engine.spaceS;
        wrapMode: TextInput.WrapAtWordBoundaryOrAnywhere;
        readOnly: !_self.editable;
        selectByMouse: true;
        selectionColor: Engine.bgHighlight;
        activeFocusOnTab: true;
        inputMethodHints: (Qt.ImhNoPredictiveText | Qt.ImhSensitiveData | Qt.ImhNoAutoUppercase);
        selectedTextColor: Engine.fgSelected;
        passwordCharacter: "\u25CF";
        activeFocusOnPress: true;
        font.pixelSize: Engine.fontM;
        ExtraAnchors.horizontalFill: parent;
        onTextChanged: {
            if (_input.activeFocus) {
                _self.edited (_self.value);
            }
        }
    }
    TextMetrics {
        id: _metrics;
        font: _input.font;
    }
}
