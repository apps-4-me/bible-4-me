import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;

MouseArea {
    id: _self;
    hoverEnabled: true;
    implicitWidth: (_lbl.visible ? Math.max ((_layout.implicitWidth + _self.padding * 2), _self.implicitHeight) : _self.implicitHeight);
    implicitHeight: (_layout.implicitHeight + _self.padding * 2);

    property alias icon: _ico.icon;
    property alias value: _lbl.text;
    property color background: Engine.bgClickable;
    property color foreground: Engine.fgText;
    property bool invertLayout: false;
    property bool checked: false;
    property int padding: Engine.spaceM;
    property int textSize: Engine.fontM;

    // compat :
    property alias text: _lbl.text;
    property alias label: _lbl.text;
    property alias color: _self.background;

    function click () {
        if (_self.visible && _self.enabled) {
            _self.clicked (null);
        }
    }

    Rectangle {
        id: _back;
        color: _self.background;
        radius: Engine.spaceS;
        opacity: (_self.enabled ? (_self.checked ? 1.0 : (_self.containsPress ? 0.85 : 0.35)) : 0.0);
        anchors.fill: parent;

        Behavior on opacity {
            OpacityAnimator {
                duration: 150;
            }
        }
    }
    RowContainer {
        id: _layout;
        opacity: (_self.enabled ? 1.0 : 0.65);
        spacing: Engine.spaceM;
        horizontalDirection: (_self.invertLayout ? HorizontalDirections.RIGHT_TO_LEFT : HorizontalDirections.LEFT_TO_RIGHT);
        anchors.centerIn: parent;

        SvgIconLoader {
            id: _ico;
            color: _lbl.color;
            visible: (_self.icon !== "");
            anchors.verticalCenter: parent.verticalCenter;
        }
        TextLabel {
            id: _lbl;
            color: (_self.enabled ? _self.foreground : Engine.fgBorder);
            visible: (_self.label !== "");
            font.pixelSize: _self.textSize;
            anchors.verticalCenter: parent.verticalCenter;
        }
    }
}
