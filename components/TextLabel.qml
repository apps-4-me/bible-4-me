import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;

Text {
    id: _self;
    color: Engine.fgText;
    textFormat: Text.PlainText;
    font {
        weight: (_self.emphasis ? Font.Bold : Font.Normal);
        family: Engine.fontName;
        pixelSize: Engine.fontM;
    }

    property bool emphasis: false;
}
