import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;

Rectangle {
    id: _self;
    color: Engine.bgSecondary;
    implicitHeight: (_container.implicitHeight + _container.anchors.margins * 2);
    ExtraAnchors.horizontalFill: parent;

    default property alias content: _container.data;

    RowContainer {
        id: _container;
        spacing: Engine.spaceM;
        anchors {
            margins: Engine.spaceM;
            verticalCenter: parent.verticalCenter;
        }
        ExtraAnchors.horizontalFill: parent;
    }
    Line {
        ExtraAnchors.bottomDock: parent;
    }
}
