<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>BibleEngine</name>
    <message>
        <source>1 Chronicles</source>
        <translation>1 Chronicles</translation>
    </message>
    <message>
        <source>1 Corinthians</source>
        <translation>1 Corinthians</translation>
    </message>
    <message>
        <source>1 Ezra</source>
        <translation>1 Ezra</translation>
    </message>
    <message>
        <source>1 John</source>
        <translation>1 John</translation>
    </message>
    <message>
        <source>1 Kings</source>
        <translation>1 Kings</translation>
    </message>
    <message>
        <source>1 Maccabees</source>
        <translation>1 Maccabees</translation>
    </message>
    <message>
        <source>1 Peter</source>
        <translation>1 Peter</translation>
    </message>
    <message>
        <source>1 Samuel</source>
        <translation>1 Samuel</translation>
    </message>
    <message>
        <source>1 Thessalonians</source>
        <translation>1 Thessalonians</translation>
    </message>
    <message>
        <source>1 Timothy</source>
        <translation>1 Timothy</translation>
    </message>
    <message>
        <source>2 Chronicles</source>
        <translation>2 Chronicles</translation>
    </message>
    <message>
        <source>2 Corinthians</source>
        <translation>2 Corinthians</translation>
    </message>
    <message>
        <source>2 Ezra</source>
        <translation>2 Ezra</translation>
    </message>
    <message>
        <source>2 John</source>
        <translation>2 John</translation>
    </message>
    <message>
        <source>2 Kings</source>
        <translation>2 Kings</translation>
    </message>
    <message>
        <source>2 Maccabees</source>
        <translation>2 Maccabees</translation>
    </message>
    <message>
        <source>2 Peter</source>
        <translation>2 Peter</translation>
    </message>
    <message>
        <source>2 Samuel</source>
        <translation>2 Samuel</translation>
    </message>
    <message>
        <source>2 Thessalonians</source>
        <translation>2 Thessalonians</translation>
    </message>
    <message>
        <source>2 Timothy</source>
        <translation>2 Timothy</translation>
    </message>
    <message>
        <source>3 John</source>
        <translation>3 John</translation>
    </message>
    <message>
        <source>Acts</source>
        <translation>Acts</translation>
    </message>
    <message>
        <source>Amos</source>
        <translation>Amos</translation>
    </message>
    <message>
        <source>Baruch</source>
        <translation>Baruch</translation>
    </message>
    <message>
        <source>Colossians</source>
        <translation>Colossians</translation>
    </message>
    <message>
        <source>Daniel</source>
        <translation>Daniel</translation>
    </message>
    <message>
        <source>Deuteronomy</source>
        <translation>Deuteronomy</translation>
    </message>
    <message>
        <source>Ecclesiastes</source>
        <translation>Ecclesiastes</translation>
    </message>
    <message>
        <source>Ephesians</source>
        <translation>Ephesians</translation>
    </message>
    <message>
        <source>Esther</source>
        <translation>Esther</translation>
    </message>
    <message>
        <source>Exodus</source>
        <translation>Exodus</translation>
    </message>
    <message>
        <source>Ezekiel</source>
        <translation>Ezekiel</translation>
    </message>
    <message>
        <source>Ezra</source>
        <translation>Ezra</translation>
    </message>
    <message>
        <source>Galatians</source>
        <translation>Galatians</translation>
    </message>
    <message>
        <source>Genesis</source>
        <translation>Genesis</translation>
    </message>
    <message>
        <source>Habakkuk</source>
        <translation>Habakkuk</translation>
    </message>
    <message>
        <source>Haggai</source>
        <translation>Haggai</translation>
    </message>
    <message>
        <source>Hebrews</source>
        <translation>Hebrews</translation>
    </message>
    <message>
        <source>Hosea</source>
        <translation>Hosea</translation>
    </message>
    <message>
        <source>Isaiah</source>
        <translation>Isaiah</translation>
    </message>
    <message>
        <source>James</source>
        <translation>James</translation>
    </message>
    <message>
        <source>Judith</source>
        <translation>Judith</translation>
    </message>
    <message>
        <source>Jeremiah</source>
        <translation>Jeremiah</translation>
    </message>
    <message>
        <source>Job</source>
        <translation>Job</translation>
    </message>
    <message>
        <source>Joel</source>
        <translation>Joel</translation>
    </message>
    <message>
        <source>John</source>
        <translation>John</translation>
    </message>
    <message>
        <source>Jonah</source>
        <translation>Jonah</translation>
    </message>
    <message>
        <source>Joshua</source>
        <translation>Joshua</translation>
    </message>
    <message>
        <source>Jude</source>
        <translation>Jude</translation>
    </message>
    <message>
        <source>Judges</source>
        <translation>Judges</translation>
    </message>
    <message>
        <source>Lamentations</source>
        <translation>Lamentations</translation>
    </message>
    <message>
        <source>Laodiceans</source>
        <translation>Laodiceans</translation>
    </message>
    <message>
        <source>Leviticus</source>
        <translation>Leviticus</translation>
    </message>
    <message>
        <source>Luke</source>
        <translation>Luke</translation>
    </message>
    <message>
        <source>Malachi</source>
        <translation>Malachi</translation>
    </message>
    <message>
        <source>Mark</source>
        <translation>Mark</translation>
    </message>
    <message>
        <source>Matthew</source>
        <translation>Matthew</translation>
    </message>
    <message>
        <source>Micah</source>
        <translation>Micah</translation>
    </message>
    <message>
        <source>Nahum</source>
        <translation>Nahum</translation>
    </message>
    <message>
        <source>Nehemiah</source>
        <translation>Nehemiah</translation>
    </message>
    <message>
        <source>Numbers</source>
        <translation>Numbers</translation>
    </message>
    <message>
        <source>Obadiah</source>
        <translation>Obadiah</translation>
    </message>
    <message>
        <source>Philippians</source>
        <translation>Philippians</translation>
    </message>
    <message>
        <source>Philemon</source>
        <translation>Philemon</translation>
    </message>
    <message>
        <source>Prayer of Manasseh</source>
        <translation>Prayer of Manasseh</translation>
    </message>
    <message>
        <source>Proverbs</source>
        <translation>Proverbs</translation>
    </message>
    <message>
        <source>Psalms</source>
        <translation>Psalms</translation>
    </message>
    <message>
        <source>Revelation</source>
        <translation>Revelation</translation>
    </message>
    <message>
        <source>Romans</source>
        <translation>Romans</translation>
    </message>
    <message>
        <source>Ruth</source>
        <translation>Ruth</translation>
    </message>
    <message>
        <source>Sirach</source>
        <translation>Sirach</translation>
    </message>
    <message>
        <source>Song of Solomon</source>
        <translation>Song of Solomon</translation>
    </message>
    <message>
        <source>Titus</source>
        <translation>Titus</translation>
    </message>
    <message>
        <source>Tobit</source>
        <translation>Tobit</translation>
    </message>
    <message>
        <source>Wisdom</source>
        <translation>Wisdom</translation>
    </message>
    <message>
        <source>Zechariah</source>
        <translation>Zechariah</translation>
    </message>
    <message>
        <source>Zephaniah</source>
        <translation>Zephaniah</translation>
    </message>
    <message>
        <source>v. %1</source>
        <translation>v. %1</translation>
    </message>
    <message>
        <source>chap. %1</source>
        <translation>chap. %1</translation>
    </message>
    <message>
        <source>, chap. %1</source>
        <translation>, chap. %1</translation>
    </message>
    <message>
        <source>, v. %1</source>
        <translation>, v. %1</translation>
    </message>
</context>
<context>
    <name>ConfirmRemoveBible</name>
    <message>
        <source>Remove this Bible version ?</source>
        <translation>Remove this Bible version ?</translation>
    </message>
    <message>
        <source>No, cancel</source>
        <translation>No, cancel</translation>
    </message>
    <message>
        <source>Yes, remove</source>
        <translation>Yes, remove</translation>
    </message>
</context>
<context>
    <name>DialogVerse</name>
    <message>
        <source>Selected verse</source>
        <translation>Selected verse</translation>
    </message>
    <message>
        <source>Copy reference only</source>
        <translation>Copy reference only</translation>
    </message>
    <message>
        <source>Remove from bookmarks</source>
        <translation>Remove from bookmarks</translation>
    </message>
    <message>
        <source>Add to bookmarks</source>
        <translation>Add to bookmarks</translation>
    </message>
    <message>
        <source>Copy all (text, reference, and version)</source>
        <translation>Copy all (text, reference, and version)</translation>
    </message>
    <message>
        <source>Copy text only</source>
        <translation>Copy text only</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>ExpandCollapseBar</name>
    <message>
        <source>Expand all</source>
        <translation>Expand all</translation>
    </message>
    <message>
        <source>Collapse all</source>
        <translation>Collapse all</translation>
    </message>
</context>
<context>
    <name>PageFavorites</name>
    <message>
        <source>Back to view</source>
        <translation>Back to view</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation>Favorites</translation>
    </message>
    <message>
        <source>%1 favorites</source>
        <translation>%1 favorites</translation>
    </message>
    <message>
        <source>%1 favorite</source>
        <translation>%1 favorite</translation>
    </message>
</context>
<context>
    <name>PageSearch</name>
    <message>
        <source>Back to view</source>
        <translation>Back to view</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Search</translation>
    </message>
    <message>
        <source>Full Bible</source>
        <translation>Full Bible</translation>
    </message>
    <message>
        <source>Current book</source>
        <translation>Current book</translation>
    </message>
    <message>
        <source>Current chapter</source>
        <translation>Current chapter</translation>
    </message>
    <message>
        <source>Keyword or name...</source>
        <translation>Keyword or name...</translation>
    </message>
    <message>
        <source>%1 results</source>
        <translation>%1 results</translation>
    </message>
    <message>
        <source>%1 result</source>
        <translation>%1 result</translation>
    </message>
    <message>
        <source>no search done</source>
        <translation>no search done</translation>
    </message>
</context>
<context>
    <name>PageSelect</name>
    <message>
        <source>Select</source>
        <translation>Jump to</translation>
    </message>
    <message>
        <source>Back to view</source>
        <translation>Back to view</translation>
    </message>
</context>
<context>
    <name>PageSettings</name>
    <message>
        <source>Back to view</source>
        <translation>Back to view</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <source>Night mode</source>
        <translation>Night mode</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
    <message>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <source>Text size</source>
        <translation>Text size</translation>
    </message>
    <message>
        <source>Small</source>
        <translation>Small</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>Medium</translation>
    </message>
    <message>
        <source>Large</source>
        <translation>Large</translation>
    </message>
    <message>
        <source>Extra Small</source>
        <translation>Extra Small</translation>
    </message>
    <message>
        <source>Extra Large</source>
        <translation>Extra Large</translation>
    </message>
</context>
<context>
    <name>PageVersion</name>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>Back to view</source>
        <translation>Back to view</translation>
    </message>
    <message>
        <source>Refresh index</source>
        <translation>Refresh index</translation>
    </message>
    <message>
        <source>Show only local</source>
        <translation>Only installed</translation>
    </message>
    <message>
        <source>No local Bible,
please disable filter
and download at least one.</source>
        <translation>No Bible installed,
please disable filter
and download at least one.</translation>
    </message>
    <message>
        <source>No index of Bibles,
please refresh index.</source>
        <translation>No index of Bibles,
please refresh index.</translation>
    </message>
</context>
<context>
    <name>PageView</name>
    <message>
        <source>No translation selected,
please go and add one.</source>
        <translation>No translation selected,
please go and add one.</translation>
    </message>
    <message>
        <source>No verse selected,
please choose a target.</source>
        <translation>No verse selected,
please choose a target.</translation>
    </message>
    <message>
        <source>This book and/or chapter
doesn&apos;t exist in this translation.</source>
        <translation>This book and/or chapter
doesn&apos;t exist in this translation.</translation>
    </message>
    <message>
        <source>Previous chapter</source>
        <translation>Prev. chapter</translation>
    </message>
    <message>
        <source>Next chapter</source>
        <translation>Next chapter</translation>
    </message>
    <message>
        <source>no version</source>
        <translation>no version</translation>
    </message>
</context>
<context>
    <name>ui</name>
    <message>
        <source>Bible&apos;me</source>
        <translation>Bible&apos;me</translation>
    </message>
    <message>
        <source>Refreshing index...</source>
        <translation>Refreshing index...</translation>
    </message>
    <message>
        <source>Searching...</source>
        <translation>Searching...</translation>
    </message>
</context>
</TS>
