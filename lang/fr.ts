<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>BibleEngine</name>
    <message>
        <source>1 Chronicles</source>
        <translation>1 Chroniques</translation>
    </message>
    <message>
        <source>1 Corinthians</source>
        <translation>1 Corinthiens</translation>
    </message>
    <message>
        <source>1 Ezra</source>
        <translation>1 Esdras</translation>
    </message>
    <message>
        <source>1 John</source>
        <translation>1 Jean</translation>
    </message>
    <message>
        <source>1 Kings</source>
        <translation>1 Rois</translation>
    </message>
    <message>
        <source>1 Maccabees</source>
        <translation>1 Maccabées</translation>
    </message>
    <message>
        <source>1 Peter</source>
        <translation>1 Pierre</translation>
    </message>
    <message>
        <source>1 Samuel</source>
        <translation>1 Samuel</translation>
    </message>
    <message>
        <source>1 Thessalonians</source>
        <translation>1 Thessaloniciens</translation>
    </message>
    <message>
        <source>1 Timothy</source>
        <translation>1 Timothé</translation>
    </message>
    <message>
        <source>2 Chronicles</source>
        <translation>2 Chroniques</translation>
    </message>
    <message>
        <source>2 Corinthians</source>
        <translation>2 Corinthiens</translation>
    </message>
    <message>
        <source>2 Ezra</source>
        <translation>2 Esdras</translation>
    </message>
    <message>
        <source>2 John</source>
        <translation>2 Jean</translation>
    </message>
    <message>
        <source>2 Kings</source>
        <translation>2 Rois</translation>
    </message>
    <message>
        <source>2 Maccabees</source>
        <translation>2 Maccabées</translation>
    </message>
    <message>
        <source>2 Peter</source>
        <translation>2 Pierre</translation>
    </message>
    <message>
        <source>2 Samuel</source>
        <translation>2 Samuel</translation>
    </message>
    <message>
        <source>2 Thessalonians</source>
        <translation>2 Thessaloniciens</translation>
    </message>
    <message>
        <source>2 Timothy</source>
        <translation>2 Timothé</translation>
    </message>
    <message>
        <source>3 John</source>
        <translation>3 John</translation>
    </message>
    <message>
        <source>Acts</source>
        <translation>Actes</translation>
    </message>
    <message>
        <source>Amos</source>
        <translation>Amos</translation>
    </message>
    <message>
        <source>Baruch</source>
        <translation>Baruch</translation>
    </message>
    <message>
        <source>Colossians</source>
        <translation>Colossiens</translation>
    </message>
    <message>
        <source>Daniel</source>
        <translation>Daniel</translation>
    </message>
    <message>
        <source>Deuteronomy</source>
        <translation>Deutéronome</translation>
    </message>
    <message>
        <source>Ecclesiastes</source>
        <translation>Ecclésiastes</translation>
    </message>
    <message>
        <source>Ephesians</source>
        <translation>Ephésiens</translation>
    </message>
    <message>
        <source>Esther</source>
        <translation>Esther</translation>
    </message>
    <message>
        <source>Exodus</source>
        <translation>Exode</translation>
    </message>
    <message>
        <source>Ezekiel</source>
        <translation>Ezéchiel</translation>
    </message>
    <message>
        <source>Ezra</source>
        <translation>Esdras</translation>
    </message>
    <message>
        <source>Galatians</source>
        <translation>Galates</translation>
    </message>
    <message>
        <source>Genesis</source>
        <translation>Genèse</translation>
    </message>
    <message>
        <source>Habakkuk</source>
        <translation>Habaquq</translation>
    </message>
    <message>
        <source>Haggai</source>
        <translation>Haggée</translation>
    </message>
    <message>
        <source>Hebrews</source>
        <translation>Hébreux</translation>
    </message>
    <message>
        <source>Hosea</source>
        <translation>Osée</translation>
    </message>
    <message>
        <source>Isaiah</source>
        <translation>Esaïe</translation>
    </message>
    <message>
        <source>James</source>
        <translation>Jacques</translation>
    </message>
    <message>
        <source>Judith</source>
        <translation>Judith</translation>
    </message>
    <message>
        <source>Jeremiah</source>
        <translation>Jérémie</translation>
    </message>
    <message>
        <source>Job</source>
        <translation>Job</translation>
    </message>
    <message>
        <source>Joel</source>
        <translation>Joël</translation>
    </message>
    <message>
        <source>John</source>
        <translation>Jean</translation>
    </message>
    <message>
        <source>Jonah</source>
        <translation>Jonas</translation>
    </message>
    <message>
        <source>Joshua</source>
        <translation>Josué</translation>
    </message>
    <message>
        <source>Jude</source>
        <translation>Jude</translation>
    </message>
    <message>
        <source>Judges</source>
        <translation>Juges</translation>
    </message>
    <message>
        <source>Lamentations</source>
        <translation>Lamentations</translation>
    </message>
    <message>
        <source>Laodiceans</source>
        <translation>Laodicéens</translation>
    </message>
    <message>
        <source>Leviticus</source>
        <translation>Lévitique</translation>
    </message>
    <message>
        <source>Luke</source>
        <translation>Luc</translation>
    </message>
    <message>
        <source>Malachi</source>
        <translation>Malachie</translation>
    </message>
    <message>
        <source>Mark</source>
        <translation>Marc</translation>
    </message>
    <message>
        <source>Matthew</source>
        <translation>Matthieu</translation>
    </message>
    <message>
        <source>Micah</source>
        <translation>Michée</translation>
    </message>
    <message>
        <source>Nahum</source>
        <translation>Nahum</translation>
    </message>
    <message>
        <source>Nehemiah</source>
        <translation>Néhémie</translation>
    </message>
    <message>
        <source>Numbers</source>
        <translation>Nombres</translation>
    </message>
    <message>
        <source>Obadiah</source>
        <translation>Abdias</translation>
    </message>
    <message>
        <source>Philippians</source>
        <translation>Philippiens</translation>
    </message>
    <message>
        <source>Philemon</source>
        <translation>Philémon</translation>
    </message>
    <message>
        <source>Prayer of Manasseh</source>
        <translation>Prière de Manassée</translation>
    </message>
    <message>
        <source>Proverbs</source>
        <translation>Proverbes</translation>
    </message>
    <message>
        <source>Psalms</source>
        <translation>Psaumes</translation>
    </message>
    <message>
        <source>Revelation</source>
        <translation>Apocalypse</translation>
    </message>
    <message>
        <source>Romans</source>
        <translation>Romains</translation>
    </message>
    <message>
        <source>Ruth</source>
        <translation>Ruth</translation>
    </message>
    <message>
        <source>Sirach</source>
        <translation>Sirach</translation>
    </message>
    <message>
        <source>Song of Solomon</source>
        <translation>Cantique des cantiques</translation>
    </message>
    <message>
        <source>Titus</source>
        <translation>Tite</translation>
    </message>
    <message>
        <source>Tobit</source>
        <translation>Tobie</translation>
    </message>
    <message>
        <source>Wisdom</source>
        <translation>Sagesse</translation>
    </message>
    <message>
        <source>Zechariah</source>
        <translation>Zacharie</translation>
    </message>
    <message>
        <source>Zephaniah</source>
        <translation>Sophonie</translation>
    </message>
    <message>
        <source>v. %1</source>
        <translation>v. %1</translation>
    </message>
    <message>
        <source>chap. %1</source>
        <translation>chap. %1</translation>
    </message>
    <message>
        <source>, chap. %1</source>
        <translation>, chap. %1</translation>
    </message>
    <message>
        <source>, v. %1</source>
        <translation>, v. %1</translation>
    </message>
</context>
<context>
    <name>ConfirmRemoveBible</name>
    <message>
        <source>Remove this Bible version ?</source>
        <translation>Supprimer cette version de la Bible ?</translation>
    </message>
    <message>
        <source>No, cancel</source>
        <translation>Non, annuler</translation>
    </message>
    <message>
        <source>Yes, remove</source>
        <translation>Oui, supprimer</translation>
    </message>
</context>
<context>
    <name>DialogVerse</name>
    <message>
        <source>Selected verse</source>
        <translation>Verset sélectionné</translation>
    </message>
    <message>
        <source>Copy reference only</source>
        <translation>Copier la référence seulement</translation>
    </message>
    <message>
        <source>Remove from bookmarks</source>
        <translation>Retirer des favoris</translation>
    </message>
    <message>
        <source>Add to bookmarks</source>
        <translation>Ajouter aux favoris</translation>
    </message>
    <message>
        <source>Copy all (text, reference, and version)</source>
        <translation>Copier tout (texte, référence, et version)</translation>
    </message>
    <message>
        <source>Copy text only</source>
        <translation>Copier le texte seulement</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>ExpandCollapseBar</name>
    <message>
        <source>Expand all</source>
        <translation>Tout déplier</translation>
    </message>
    <message>
        <source>Collapse all</source>
        <translation>Tout replier</translation>
    </message>
</context>
<context>
    <name>PageFavorites</name>
    <message>
        <source>Back to view</source>
        <translation>Retour à la lecture</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation>Favoris</translation>
    </message>
    <message>
        <source>%1 favorites</source>
        <translation>%1 favoris</translation>
    </message>
    <message>
        <source>%1 favorite</source>
        <translation>%1 favori</translation>
    </message>
</context>
<context>
    <name>PageSearch</name>
    <message>
        <source>Back to view</source>
        <translation>Retour à la lecture</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Recherche</translation>
    </message>
    <message>
        <source>Full Bible</source>
        <translation>Bible complète</translation>
    </message>
    <message>
        <source>Current book</source>
        <translation>Livre en cours</translation>
    </message>
    <message>
        <source>Current chapter</source>
        <translation>Chapitre en cours</translation>
    </message>
    <message>
        <source>Keyword or name...</source>
        <translation>Mot-clé ou nom...</translation>
    </message>
    <message>
        <source>%1 results</source>
        <translation>%1 résultats</translation>
    </message>
    <message>
        <source>%1 result</source>
        <translation>%1 résulat</translation>
    </message>
    <message>
        <source>no search done</source>
        <translation>aucune recherche</translation>
    </message>
</context>
<context>
    <name>PageSelect</name>
    <message>
        <source>Select</source>
        <translation>Aller à</translation>
    </message>
    <message>
        <source>Back to view</source>
        <translation>Retour à la lecture</translation>
    </message>
</context>
<context>
    <name>PageSettings</name>
    <message>
        <source>Back to view</source>
        <translation>Retour à la lecture</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Night mode</source>
        <translation>Mode nuit</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <source>Text size</source>
        <translation>Taille du texte</translation>
    </message>
    <message>
        <source>Small</source>
        <translation>Petit</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>Moyen</translation>
    </message>
    <message>
        <source>Large</source>
        <translation>Grand</translation>
    </message>
    <message>
        <source>Extra Small</source>
        <translation>Très petit</translation>
    </message>
    <message>
        <source>Extra Large</source>
        <translation>Très grand</translation>
    </message>
</context>
<context>
    <name>PageVersion</name>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>Back to view</source>
        <translation>Retour à la lecture</translation>
    </message>
    <message>
        <source>Refresh index</source>
        <translation>Mettre à jour index</translation>
    </message>
    <message>
        <source>Show only local</source>
        <translation>Installées seulement</translation>
    </message>
    <message>
        <source>No local Bible,
please disable filter
and download at least one.</source>
        <translation>Aucune Bible installée,
merci de désactiver le filtrage
et d&apos;en télécharger au moins une.</translation>
    </message>
    <message>
        <source>No index of Bibles,
please refresh index.</source>
        <translation>Pas d&apos;index des Bibles,
merci de mettre à jour l&apos;index.</translation>
    </message>
</context>
<context>
    <name>PageView</name>
    <message>
        <source>No translation selected,
please go and add one.</source>
        <translation>Pas de traduction sélectionnée,
merci d&apos;en ajouter une.</translation>
    </message>
    <message>
        <source>No verse selected,
please choose a target.</source>
        <translation>Pas de verset sélectionné,
merci d&apos;en choisir un.</translation>
    </message>
    <message>
        <source>This book and/or chapter
doesn&apos;t exist in this translation.</source>
        <translation>Ce livre et/ou chapitre
n&apos;existe pas dans cette traduction.</translation>
    </message>
    <message>
        <source>Previous chapter</source>
        <translation>Chap. précédent</translation>
    </message>
    <message>
        <source>Next chapter</source>
        <translation>Chap. suivant</translation>
    </message>
    <message>
        <source>no version</source>
        <translation>pas de version</translation>
    </message>
</context>
<context>
    <name>ui</name>
    <message>
        <source>Bible&apos;me</source>
        <translation>Bible&apos;me</translation>
    </message>
    <message>
        <source>Refreshing index...</source>
        <translation>Mise à jour index...</translation>
    </message>
    <message>
        <source>Searching...</source>
        <translation>Recherche en cours...</translation>
    </message>
</context>
</TS>
