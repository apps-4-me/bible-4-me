#pragma once

#include <QAbstractListModel>
#include <QObject>

class ObjectListModelBase : public QAbstractListModel {
    Q_OBJECT
    Q_PROPERTY (int length READ rowCount NOTIFY lengthChanged)

public:
    explicit ObjectListModelBase (QObject * parent = nullptr)
        : QAbstractListModel { parent }
    { }

    Q_INVOKABLE virtual QObject *   at (const int       idx) const = 0;
    Q_INVOKABLE virtual QObject * find (const QString & uid) const = 0;

    Q_INVOKABLE virtual void clear (void) = 0;

signals:
    void lengthChanged (void);

public slots:
    virtual void onTriggerActivated (void) = 0;
};

template<class T> class ObjectListModel : public ObjectListModelBase {
public:
    using cpp_list_type  = QVector<T *>;
    using cpp_hash_type  = QHash<QString, T *>;
    using cpp_uid_getter = const QString & (T:: *) (void) const;
    using cpp_notifier   = void (T:: *) (void);

    explicit ObjectListModel (std::initializer_list<T *> items, cpp_uid_getter uidGetter, QObject * parent = nullptr)
        : ObjectListModelBase { parent }
        , m_uidGetter         { uidGetter }
        , m_items             { items }
    { }

    void registerRole (cpp_notifier notifier) {
        m_triggers.append (notifier);
    }

    void fill (const cpp_list_type & list) {
        beginResetModel ();
        m_index.clear ();
        for (T * item : m_items) {
            if (item->parent () == this) {
                item->deleteLater ();
            }
        }
        m_items.clear ();
        if (!list.isEmpty ()) {
            m_items.reserve (list.count ());
            for (T * item : list) {
                if (item != nullptr) {
                    m_items.append (item);
                    link (item);
                    if (m_uidGetter != nullptr) {
                        m_index.insert ((item->*m_uidGetter) (), item);
                    }
                    if (item->parent () == nullptr) {
                        item->setParent (this);
                    }
                }
            }
        }
        endResetModel ();
        emit lengthChanged ();
    }

    void append (T * item) {
        if (item != nullptr) {
            const int idx { m_items.count () };
            beginInsertRows (QModelIndex { }, idx, idx);
            m_items.append (item);
            link (item);
            if (m_uidGetter != nullptr) {
                m_index.insert ((item->*m_uidGetter) (), item);
            }
            if (item->parent () == nullptr) {
                item->setParent (this);
            }
            endInsertRows ();
            emit lengthChanged ();
        }
    }

    void prepend (T * item) {
        if (item != nullptr) {
            beginInsertRows (QModelIndex { }, 0, 0);
            m_items.prepend (item);
            link (item);
            if (m_uidGetter != nullptr) {
                m_index.insert ((item->*m_uidGetter) (), item);
            }
            if (item->parent () == nullptr) {
                item->setParent (this);
            }
            endInsertRows ();
            emit lengthChanged ();
        }
    }

    void insert (T * item, const int idx) {
        if (item != nullptr) {
            beginInsertRows (QModelIndex { }, idx, idx);
            m_items.insert (idx, item);
            link (item);
            if (m_uidGetter != nullptr) {
                m_index.insert ((item->*m_uidGetter) (), item);
            }
            if (item->parent () == nullptr) {
                item->setParent (this);
            }
            endInsertRows ();
            emit lengthChanged ();
        }
    }

    void remove (T * item) {
        if (item != nullptr) {
            const int idx { m_items.indexOf (item) };
            if (idx >= 0) {
                beginRemoveRows (QModelIndex { }, idx, idx);
                if (m_uidGetter != nullptr) {
                    m_index.remove ((item->*m_uidGetter) ());
                }
                m_items.remove (idx);
                if (item->parent () == this) {
                    item->deleteLater ();
                }
                endRemoveRows ();
                emit lengthChanged ();
            }
        }
    }

    void clear (void) final {
        if (!m_items.isEmpty ()) {
            beginResetModel ();
            m_index.clear ();
            for (T * item : m_items) {
                if (item->parent () == this) {
                    item->deleteLater ();
                }
            }
            m_items.clear ();
            endResetModel ();
            emit lengthChanged ();
        }
    }

    inline bool isEmpty  (void)     const { return m_items.isEmpty (); }
    inline bool contains (T * item) const { return m_items.contains (item); }
    inline int  indexOf  (T * item) const { return m_items.indexOf (item); }
    inline int  length   (void)     const { return m_items.length (); }

    inline T * get        (const int       idx) const { return ((idx >= 0 && idx < m_items.length ()) ? m_items.at (idx) : nullptr); }
    inline T * operator[] (const int       idx) const { return get (idx); }
    inline T * getByUid   (const QString & uid) const { return m_index.value (uid, nullptr); }

    QObject * at (const int idx) const final {
        return get (idx);
    }

    QObject * find (const QString & uid) const final {
        return getByUid (uid);
    }

    const cpp_list_type & toVector (void) const {
        return m_items;
    }

    using Iterator = typename cpp_list_type::const_iterator;
    inline Iterator begin (void) const { return m_items.begin (); }
    inline Iterator end   (void) const { return m_items.end ();   }

    inline T * first (void) const { return (!m_items.isEmpty () ? m_items.first () : nullptr); }
    inline T * last (void)  const { return (!m_items.isEmpty () ? m_items.last ()  : nullptr); }

protected:
    QHash<int, QByteArray> roleNames (void) const final {
        return {
            { Qt::ItemDataRole::UserRole, QByteArrayLiteral ("modelData") },
        };
    }

    int rowCount (const QModelIndex & parent = { }) const final {
        return (!parent.isValid () ? m_items.count () : 0);
    }

    QVariant data (const QModelIndex & index, int role) const final {
        return QVariant::fromValue ((role == Qt::ItemDataRole::UserRole) ? get (index.row ()) : nullptr);
    }

    void link (T * item) {
        if (item != nullptr) {
            for (cpp_notifier notifier : m_triggers) {
                connect (item, notifier, this, &ObjectListModelBase::onTriggerActivated);
            }
        }
    }

    void onTriggerActivated (void) final {
        if (T * item { qobject_cast<T *> (sender ()) }) {
            const int idx { m_items.indexOf (item) };
            if (idx >= 0) {
                emit dataChanged (index (idx), index (idx), { Qt::ItemDataRole::DisplayRole });
            }
        }
    }

private:
    QVector<cpp_notifier> m_triggers;
    cpp_uid_getter m_uidGetter;
    cpp_list_type m_items;
    cpp_hash_type m_index;
};

#define QML_OBJ_LIST_MODEL_PROPERTY(NAME, TYPE) \
    protected: ObjectListModel<TYPE> m_##NAME; \
    public: ObjectListModel<TYPE> & NAME (void) { return m_##NAME; } \
    public: const ObjectListModel<TYPE> & NAME (void) const { return m_##NAME; } \
    public: QAbstractListModel * get_##NAME (void) { return &m_##NAME; } \
    private: Q_PROPERTY (QAbstractListModel * NAME READ get_##NAME CONSTANT)

struct __FORCE_MOC__ { Q_GADGET };
