
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QSettings>
#include <QUrl>
#include <qqml.h>

#include "BibleEngine.h"
#include "QQmlContainerEnums.h"
#include "QQuickColumnContainer.h"
#include "QQuickContainerAttachedObject.h"
#include "QQuickExtraAnchors.h"
#include "QQuickRowContainer.h"

#define QML_MODULE_URI "BibleMe", 1, 0

static void computeZoomRatio (QQuickWindow * window) {
    BibleEngine::instance ().set_zoomRatio (1.15 * window->width () / 480.0);
}

int main (int argc, char * argv []) {
    static const QString ERR { QStringLiteral ("!!!") };
    QGuiApplication::setApplicationName  (QStringLiteral ("BibleMe"));
    QGuiApplication::setOrganizationName (QStringLiteral ("UniqueConception"));
    QGuiApplication app { argc, argv };
    qmlRegisterType<QQuickColumnContainer>                    (QML_MODULE_URI, "ColumnContainer");
    qmlRegisterType<QQuickRowContainer>                       (QML_MODULE_URI, "RowContainer");
    qmlRegisterUncreatableType<ObjectListModelBase>           (QML_MODULE_URI, "ObjectListModel",      ERR);
    qmlRegisterUncreatableType<QQuickContainerAttachedObject> (QML_MODULE_URI, "Container",            ERR);
    qmlRegisterUncreatableType<QQuickExtraAnchors>            (QML_MODULE_URI, "ExtraAnchors",         ERR);
    qmlRegisterUncreatableType<FlowDirections>                (QML_MODULE_URI, "FlowDirections",       ERR);
    qmlRegisterUncreatableType<HorizontalDirections>          (QML_MODULE_URI, "HorizontalDirections", ERR);
    qmlRegisterUncreatableType<VerticalDirections>            (QML_MODULE_URI, "VerticalDirections",   ERR);
    qmlRegisterUncreatableType<BibleLanguage>                 (QML_MODULE_URI, "BibleLanguage",        ERR);
    qmlRegisterUncreatableType<BibleText>                     (QML_MODULE_URI, "BibleText",            ERR);
    qmlRegisterUncreatableType<BibleBook>                     (QML_MODULE_URI, "BibleBook",            ERR);
    qmlRegisterUncreatableType<BibleChapter>                  (QML_MODULE_URI, "BibleChapter",         ERR);
    qmlRegisterUncreatableType<BibleVerse>                    (QML_MODULE_URI, "BibleVerse",           ERR);
    qmlRegisterUncreatableType<BibleGroup>                    (QML_MODULE_URI, "BibleGroup",           ERR);
    qmlRegisterUncreatableType<BibleScope>                    (QML_MODULE_URI, "BibleScope",           ERR);
    qmlRegisterUncreatableType<ObjectListModelBase>           (QML_MODULE_URI, "ObjectListModel",      ERR);
    qmlRegisterUncreatableType<PageIds>                       (QML_MODULE_URI, "PageIds",              ERR);
    qmlRegisterUncreatableType<ZoomLevel>                     (QML_MODULE_URI, "ZoomLevel",            ERR);
    qmlRegisterSingletonType<BibleEngine>                     (QML_MODULE_URI, "Engine", [] (QQmlEngine *, QJSEngine *) -> QObject * {
        return &BibleEngine::instance ();
    });
    QQmlApplicationEngine engine { QUrl { QStringLiteral ("qrc:///ui.qml") } };
    const QObjectList rootObjects { engine.rootObjects () };
    for (QObject * object : rootObjects) {
        if (QQuickWindow * window { qobject_cast<QQuickWindow *> (object) }) {
            QObject::connect (window, &QQuickWindow::widthChanged, window, [window] (void) {
                computeZoomRatio (window);
            });
            computeZoomRatio (window);
        }
    }
    return QGuiApplication::exec ();
}

