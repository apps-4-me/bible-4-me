import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;
import "../components";

ModalDialog {
    id: _self;

    property BibleText bibleText: null;

    TextLabel {
        text: $ (qsTr ("Remove this Bible version ?"));
        color: Engine.fgSelected;
        wrapMode: Text.Wrap;
        horizontalAlignment: Text.AlignHCenter;
        font.pixelSize: Engine.fontL;
        ExtraAnchors.horizontalFill: parent;
    }
    TextLabel {
        text: "“ %1 ”".arg (_self.bibleText.bibleTitle);
        wrapMode: Text.Wrap;
        horizontalAlignment: Text.AlignHCenter;
        ExtraAnchors.horizontalFill: parent;
    }
    Line {
        ExtraAnchors.horizontalFill: parent;
    }
    RowContainer {
        ExtraAnchors.horizontalFill: parent;

        TextButton {
            icon: "qrc:///img/cancel.svg";
            label: $ (qsTr ("No, cancel"));
            onClicked: {
                _self.hide ();
            }
        }
        Stretcher { }
        TextButton {
            icon: "qrc:///img/ok.svg";
            label: $ (qsTr ("Yes, remove"));
            invertLayout: true;
            onClicked: {
                Engine.removeText (_self.bibleText.textKey);
                _self.hide ();
            }
        }
    }
}
