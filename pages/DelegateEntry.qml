import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;
import "../components";

SwipeDetector {
    id: _self;
    selected: (_self.qtObject === Engine.currentVerse);
    implicitHeight: (_line.implicitHeight + _line.anchors.margins * 2);
    onClicked: {
        Engine.selectPosition (_self.qtObject.verseId);
        Engine.currentPage = PageIds.VIEW;
    }
    onPressAndHold: {
        _self.contextActionsRequested (_self.qtObject);
    }

    property BibleVerse qtObject: null;

    signal contextActionsRequested (BibleVerse verse);

    ColumnContainer {
        id: _line;
        anchors {
            margins: Engine.spaceS;
            leftMargin: ((20 * Engine.zoomRatio) + Engine.spaceM * 2);
            verticalCenter: parent.verticalCenter;
        }
        ExtraAnchors.horizontalFill: parent;

        TextLabel {
            text: $ (Engine.formatReference (_self.qtObject.verseId, false)) + " :";
            color: Engine.fgSelected;
            font.pixelSize: (Engine.currentTextSize * 0.85);
            ExtraAnchors.horizontalFill: parent;
        }
        TextLabel {
            text: _self.qtObject.textContent;
            wrapMode: Text.Wrap;
            font.pixelSize: Engine.currentTextSize;
            ExtraAnchors.horizontalFill: parent;
        }
    }
}
