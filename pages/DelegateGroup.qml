import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;
import "../components";

ColumnContainer {
    id: _self;
    visible: _self.qtObject.entriesModel.length;
    ExtraAnchors.horizontalFill: parent;

    property BibleGroup qtObject: null;

    signal contextActionsRequested (BibleVerse verse);

    SwipeDetector {
        id: _clicker;
        implicitHeight: (_header.implicitHeight + _header.anchors.margins * 2);
        onClicked: {
            _self.qtObject.expanded = !_self.qtObject.expanded;
        }

        RowContainer {
            id: _header;
            spacing: Engine.spaceM;
            anchors {
                margins: Engine.spaceM;
                verticalCenter: parent.verticalCenter;
            }
            ExtraAnchors.horizontalFill: parent;

            SvgIconLoader {
                icon: (_self.qtObject.expanded ? "qrc:///img/folder-opened.svg" : "qrc:///img/folder-closed.svg");
                anchors.verticalCenter: parent.verticalCenter;
            }
            TextLabel {
                text: Engine.formatReference (_self.qtObject.bookId);
                font.pixelSize: Engine.fontL;
                anchors.verticalCenter: parent.verticalCenter;
                Container.horizontalStretch: 1;
            }
            Item {
                id: _bubble;
                implicitWidth: Math.max (_counter.implicitWidth, _counter.implicitHeight);
                implicitHeight: _counter.implicitHeight;
                anchors.verticalCenter: parent.verticalCenter;

                Rectangle {
                    color: Engine.bgHighlight;
                    radius: (_bubble.width * 0.5);
                    opacity: 0.65;
                    anchors.fill: parent;
                }
                TextLabel {
                    id: _counter;
                    text: _self.qtObject.entriesModel.length;
                    color: Engine.fgSelected;
                    padding: Engine.spaceS;
                    emphasis: true;
                    anchors.centerIn: parent;
                }
            }
        }
    }
    ColumnContainer {
        visible: _self.qtObject.expanded;
        ExtraAnchors.horizontalFill: parent;

        Repeater {
            id: repeaterFavorites;
            model: (_self.qtObject.expanded ? _self.qtObject.entriesModel : 0);
            delegate: DelegateEntry {
                qtObject: (modelData || null);
                onContextActionsRequested: {
                    _self.contextActionsRequested (verse);
                }
            }
        }
    }
}
