import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;
import "../components";

SwipeDetector {
    id: _self;
    implicitHeight: (_lbl.height + _lbl.anchors.margins * 2);
    onClicked: {
        Engine.selectPosition (_self.refId);
        _self.triggered ();
    }

    property string refId: "";

    signal triggered ();

    TextLabel {
        id: _lbl;
        text: $ (Engine.formatReference (_self.refId, true));
        color: (_lbl.emphasis ? Engine.fgSelected : Engine.fgText);
        emphasis: _self.selected;
        fontSizeMode: Text.HorizontalFit;
        horizontalAlignment: Text.AlignHCenter;
        anchors {
            margins: Engine.spaceM;
            verticalCenter: parent.verticalCenter;
        }
        ExtraAnchors.horizontalFill: parent;
    }
}
