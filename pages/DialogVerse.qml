import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;
import "../components";

ModalDialog {
    id: _self;

    property BibleVerse bibleVerse: null;

    readonly property string translation: (Engine.currentText ? Engine.currentText.bibleTitle : "");
    readonly property string reference: $ (_self.bibleVerse ? Engine.formatReference (_self.bibleVerse.verseId) : "");
    readonly property string quote: (_self.bibleVerse ? _self.bibleVerse.textContent : null);

    function copyInClipboard (str) {
        _input.text = str;
        _input.selectAll ();
        _input.copy ();
    }

    TextInput {
        id: _input;
        visible: false;
    }
    TextLabel {
        text: $ (qsTr ("Selected verse"));
        color: Engine.fgSelected;
        wrapMode: Text.Wrap;
        horizontalAlignment: Text.AlignHCenter;
        font.pixelSize: Engine.fontL;
        ExtraAnchors.horizontalFill: parent;
    }
    TextLabel {
        text: $ ("“ %1 ”".arg (_self.quote));
        color: Engine.fgText;
        wrapMode: Text.Wrap;
        horizontalAlignment: Text.AlignHCenter;
        font.pixelSize: Engine.fontS;
        ExtraAnchors.horizontalFill: parent;
    }
    TextLabel {
        text: $ ("— %1".arg (_self.reference));
        color: Engine.fgSelected;
        opacity: 0.65;
        wrapMode: Text.Wrap;
        horizontalAlignment: Text.AlignHCenter;
        ExtraAnchors.horizontalFill: parent;
    }
    TextLabel {
        text: $ ("(%1)".arg (_self.translation));
        color: Engine.fgText;
        opacity: 0.65;
        wrapMode: Text.Wrap;
        horizontalAlignment: Text.AlignHCenter;
        ExtraAnchors.horizontalFill: parent;
    }
    ColumnContainer {
        spacing: Engine.spaceS;
        ExtraAnchors.horizontalFill: parent;

        TextButton {
            icon: "qrc:///img/info.svg";
            text: $ (qsTr ("Copy all (text, reference, and version)"));
            ExtraAnchors.horizontalFill: parent;
            onClicked: {
                _self.copyInClipboard ("“ %1 ”\n— %2 (%3)".arg (_self.quote).arg (_self.reference).arg (_self.translation));
                _self.hide ();
            }
        }
        TextButton {
            icon: "qrc:///img/copy.svg";
            text: $ (qsTr ("Copy text only"));
            ExtraAnchors.horizontalFill: parent;
            onClicked: {
                _self.copyInClipboard (_self.quote);
                _self.hide ();
            }
        }
        TextButton {
            icon: "qrc:///img/link.svg";
            text: $ (qsTr ("Copy reference only"));
            ExtraAnchors.horizontalFill: parent;
            onClicked: {
                _self.copyInClipboard (_self.reference);
                _self.hide ();
            }
        }
        TextButton {
            icon: "qrc:///img/mark.svg";
            text: $ (_self.bibleVerse.marked ? qsTr ("Remove from bookmarks") : qsTr ("Add to bookmarks"));
            checked: _self.bibleVerse.marked;
            ExtraAnchors.horizontalFill: parent;
            onClicked: {
                if (_self.bibleVerse.marked) {
                    Engine.removeBookmark (_self.bibleVerse.verseId);
                }
                else {
                    Engine.addBookmark (_self.bibleVerse.verseId);
                }
                _self.hide ();
            }
        }
    }
    Line {
        ExtraAnchors.horizontalFill: parent;
    }
    TextButton {
        icon: "qrc:///img/cancel.svg";
        label: $ (qsTr ("Cancel"));
        anchors.horizontalCenter: parent.horizontalCenter;
        onClicked: {
            _self.hide ();
        }
    }
}
