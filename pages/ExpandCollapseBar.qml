import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;
import "../components";

StatusBar {
    id: _self;

    property ObjectListModel model: null;

    TextButton {
        icon: "qrc:///img/plus.svg";
        text: $ (qsTr ("Expand all"));
        enabled: _self.model.length;
        onClicked: {
            for (let idx = 0; idx < _self.model.length; ++idx) {
                _self.model.at (idx) ["expanded"] = true;
            }
        }
    }
    Stretcher { }
    TextButton {
        icon: "qrc:///img/minus.svg";
        text: $ (qsTr ("Collapse all"));
        enabled: _self.model.length;
        invertLayout: true;
        onClicked: {
            for (let idx = 0; idx < _self.model.length; ++idx) {
                _self.model.at (idx) ["expanded"] = false;
            }
        }
    }
}
