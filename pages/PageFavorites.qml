import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;
import "../components";

LazyPage {
    id: _self;
    needed: (Engine.currentPage === PageIds.FAVORITES);

    ColumnContainer {
        anchors.fill: parent;

        ToolBar {
            TextButton {
                text: $ (qsTr ("Back to view"));
                icon: "qrc:///img/home.svg";
                anchors.verticalCenter: parent.verticalCenter;
                onClicked: {
                    Engine.currentPage = PageIds.VIEW;
                }
            }
            Stretcher { }
            TextLabel {
                text: $ (qsTr ("Favorites"));
                font.pixelSize: Engine.fontL;
                anchors.verticalCenter: parent.verticalCenter;
            }
            SvgIconLoader {
                icon: "qrc:///img/mark.svg";
                color: Engine.fgSelected;
                anchors.verticalCenter: parent.verticalCenter;
            }
        }
        ScrollContainer {
            id: scrollerFavorites;
            ExtraAnchors.horizontalFill: parent;
            Container.verticalStretch: 1;

            Repeater {
                id: repeaterFavoriteGroups;
                model: Engine.favoritesModel;
                delegate: DelegateGroup {
                    qtObject: (modelData || null);
                    onContextActionsRequested: {
                        _self.contextActionsRequested (verse);
                    }
                }
            }
        }
        Line {
            ExtraAnchors.horizontalFill: parent;
        }
        TextLabel {
            text: $ (Engine.favoritesTotal >= 1 ? qsTr ("%1 favorites") : qsTr ("%1 favorite")).arg (Engine.favoritesTotal);
            opacity: 0.65;
            padding: Engine.spaceM;
            horizontalAlignment: Text.AlignHCenter;
            ExtraAnchors.horizontalFill: parent;
        }
        ExpandCollapseBar {
            model: Engine.favoritesModel;
        }
    }
}
