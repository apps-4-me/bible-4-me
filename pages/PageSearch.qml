import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;
import "../components";

LazyPage {
    id: _self;
    needed: (Engine.currentPage === PageIds.SEARCH);

    ColumnContainer {
        anchors.fill: parent;

        ToolBar {
            TextButton {
                icon: "qrc:///img/home.svg";
                text: $ (qsTr ("Back to view"));
                anchors.verticalCenter: parent.verticalCenter;
                onClicked: {
                    Engine.currentPage = PageIds.VIEW;
                }
            }
            Stretcher { }
            TextLabel {
                text: $ (qsTr ("Search"));
                font.pixelSize: Engine.fontL;
                anchors.verticalCenter: parent.verticalCenter;
            }
            SvgIconLoader {
                icon: "qrc:///img/find.svg";
                color: Engine.fgSelected;
                anchors.verticalCenter: parent.verticalCenter;
            }
        }
        Item {
            Container.forcedHeight: Engine.spaceM;
        }
        RowContainer {
            spacing: Engine.spaceM;
            anchors.margins: Engine.spaceM;
            ExtraAnchors.horizontalFill: parent;

            TextButton {
                id: _toggleScope;
                label: {
                    switch (Engine.currentScope) {
                    case BibleScope.FULL_BIBLE:      return $ (qsTr ("Full Bible"));
                    case BibleScope.CURRENT_BOOK:    return $ (qsTr ("Current book"));
                    case BibleScope.CURRENT_CHAPTER: return $ (qsTr ("Current chapter"));
                    }
                    return "";
                }
                anchors.verticalCenter: parent.verticalCenter;
                onClicked: {
                    _toggleScope.checked = !_toggleScope.checked;
                }
            }
            RowContainer {
                visible: _toggleScope.checked;
                spacing: Engine.spaceM;
                anchors.verticalCenter: parent.verticalCenter;

                Repeater {
                    model: [
                        { value: BibleScope.FULL_BIBLE,      label: $ (qsTr ("Full Bible")) },
                        { value: BibleScope.CURRENT_BOOK,    label: $ (qsTr ("Current book")) },
                        { value: BibleScope.CURRENT_CHAPTER, label: $ (qsTr ("Current chapter")) },
                    ];
                    delegate: TextButton {
                        value: modelData ["label"];
                        visible: (Engine.currentScope !== modelData ["value"]);
                        onClicked: {
                            Engine.currentScope = modelData ["value"];
                            _toggleScope.checked = false;
                        }
                    }
                }
            }
            TextBox {
                id: _input;
                enabled: !_toggleScope.checked;
                visible: !_toggleScope.checked;
                placeholder: $ (qsTr ("Keyword or name..."));
                anchors.verticalCenter: parent.verticalCenter;
                Container.horizontalStretch: 1;
                Keys.onReturnPressed: {
                    _btnFind.click ();
                }
                Keys.onEnterPressed: {
                    _btnFind.click ();
                }
            }
            TextButton {
                id: _btnFind;
                icon: "qrc:///img/ok.svg";
                visible: !_toggleScope.checked;
                enabled: (_input.value.trim ().length >= 3);
                anchors.verticalCenter: parent.verticalCenter;
                onClicked: {
                    _input.focus = false;
                    Engine.searchContent (_input.value);
                }
            }
        }
        Item {
            Container.forcedHeight: Engine.spaceM;
        }
        TextLabel {
            text: $ (Engine.currentSearch ? "“ %1 ”".arg (Engine.currentSearch) : qsTr ("no search done"));
            opacity: 0.65;
            anchors.horizontalCenter: parent.horizontalCenter;
        }
        Item {
            Container.forcedHeight: Engine.spaceM;
        }
        Line {
            ExtraAnchors.horizontalFill: parent;
        }
        ScrollContainer {
            id: _scroller;
            ExtraAnchors.horizontalFill: parent;
            Container.verticalStretch: 1;

            Repeater {
                model: Engine.resultsModel;
                delegate: DelegateGroup {
                    qtObject: (modelData || null);
                    onContextActionsRequested: {
                        _self.contextActionsRequested (verse);
                    }
                }
            }
        }
        Line {
            ExtraAnchors.horizontalFill: parent;
        }
        TextLabel {
            text: $ (Engine.resultsTotal >= 1 ? qsTr ("%1 results") : qsTr ("%1 result")).arg (Engine.resultsTotal);
            opacity: 0.65;
            padding: Engine.spaceM;
            horizontalAlignment: Text.AlignHCenter;
            ExtraAnchors.horizontalFill: parent;
        }
        ExpandCollapseBar {
            model: Engine.resultsModel;
        }
    }
}
