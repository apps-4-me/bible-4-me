import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;
import "../components";

LazyPage {
    id: _self;
    needed: (Engine.currentPage === PageIds.SELECTOR);

    ColumnContainer {
        anchors.fill: parent;

        ToolBar {
            SvgIconLoader {
                icon: "qrc:///img/target.svg";
                color: Engine.fgSelected;
                anchors.verticalCenter: parent.verticalCenter;
            }
            TextLabel {
                text: $ (qsTr ("Select"));
                font.pixelSize: Engine.fontL;
                anchors.verticalCenter: parent.verticalCenter;
            }
            Stretcher { }
            TextButton {
                icon: "qrc:///img/home.svg";
                text: $ (qsTr ("Back to view"));
                invertLayout: true;
                anchors.verticalCenter: parent.verticalCenter;
                onClicked: {
                    Engine.currentPage = PageIds.VIEW;
                }
            }
        }
        RowContainer {
            ExtraAnchors.horizontalFill: parent;
            Container.verticalStretch: 1;

            ScrollableListView {
                model: Engine.booksModel;
                delegate: DelegateSelect {
                    id: _book;
                    refId: _book.qtObject.bookId;
                    selected: (_book.qtObject === Engine.selectorBook);

                    readonly property BibleBook qtObject: (modelData || null);
                }
                ExtraAnchors.verticalFill: parent;
                Container.horizontalStretch: 3;
            }
            Line {
                ExtraAnchors.verticalFill: parent;
            }
            ScrollableListView {
                model: (Engine.selectorBook ? Engine.selectorBook.chaptersModel : 0);
                delegate: DelegateSelect {
                    id: _chapter;
                    refId: _chapter.qtObject.chapterId;
                    selected: (_chapter.qtObject === Engine.selectorChapter);

                    readonly property BibleChapter qtObject: (modelData || null);
                }
                ExtraAnchors.verticalFill: parent;
                Container.horizontalStretch: 2;
            }
            Line {
                ExtraAnchors.verticalFill: parent;
            }
            ScrollableListView {
                model: (Engine.selectorChapter ? Engine.selectorChapter.verseModel : 0);
                delegate: DelegateSelect {
                    id: _verse;
                    refId: _verse.qtObject.verseId;
                    selected: (_verse.qtObject === Engine.selectorVerse);
                    onTriggered: {
                        Engine.currentPage = PageIds.VIEW;
                    }

                    readonly property BibleVerse qtObject: (modelData || null);
                }
                ExtraAnchors.verticalFill: parent;
                Container.horizontalStretch: 2;
            }
        }
    }
}
