import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;
import "../components";

LazyPage {
    id: _self;
    needed: (Engine.currentPage === PageIds.CONFIG);

    ColumnContainer {
        anchors.fill: parent;

        ToolBar {
            TextButton {
                icon: "qrc:///img/home.svg";
                text: $ (qsTr ("Back to view"));
                anchors.verticalCenter: parent.verticalCenter;
                onClicked: {
                    Engine.currentPage = PageIds.VIEW;
                }
            }
            Stretcher { }
            TextLabel {
                text: $ (qsTr ("Settings"));
                font.pixelSize: Engine.fontL;
                anchors.verticalCenter: parent.verticalCenter;
            }
            SvgIconLoader {
                icon: "qrc:///img/config.svg";
                color: Engine.fgSelected;
                anchors.verticalCenter: parent.verticalCenter;
            }
        }
        Item {
            ExtraAnchors.horizontalFill: parent;
            Container.verticalStretch: 1;

            ColumnContainer {
                spacing: (Engine.spaceL * 3);
                anchors {
                    top: parent.top;
                    margins: (Engine.spaceL * 3);
                    horizontalCenter: parent.horizontalCenter;
                }

                ColumnContainer {
                    spacing: Engine.spaceM;
                    anchors.horizontalCenter: parent.horizontalCenter;

                    TextLabel {
                        text: $ (qsTr ("Night mode"));
                        color: Engine.fgSelected;
                        anchors.horizontalCenter: parent.horizontalCenter;
                    }
                    RowContainer {
                        spacing: Engine.spaceM;
                        anchors.horizontalCenter: parent.horizontalCenter;

                        OnOffSwitcher {
                            anchors.verticalCenter: parent.verticalCenter;
                            onEdited: { Engine.nightMode = value; }

                            Binding on value { value: Engine.nightMode; }
                        }
                        TextLabel {
                            text: (Engine.nightMode ? $ (qsTr ("Yes")) : $ (qsTr ("No")));
                            font.pixelSize: Engine.fontS;
                            anchors.verticalCenter: parent.verticalCenter;
                        }
                    }
                }
                ColumnContainer {
                    spacing: Engine.spaceM;
                    anchors.horizontalCenter: parent.horizontalCenter;

                    TextLabel {
                        text: $ (qsTr ("Language"));
                        color: Engine.fgSelected;
                        anchors.horizontalCenter: parent.horizontalCenter;
                    }
                    RowContainer {
                        spacing: Engine.spaceM;
                        anchors.horizontalCenter: parent.horizontalCenter;

                        Repeater {
                            model: [
                                { value: "en", label: "English" },
                                { value: "fr", label: "Français" },
                            ];
                            delegate: TextButton {
                                label: modelData ["label"];
                                checked: (Engine.currentTranslationCode === modelData ["value"]);
                                onClicked: {
                                    Engine.translateUi (modelData ["value"]);
                                }
                            }
                        }
                    }
                }
                ColumnContainer {
                    spacing: Engine.spaceM;
                    anchors.horizontalCenter: parent.horizontalCenter;

                    TextLabel {
                        text: $ (qsTr ("Text size"));
                        color: Engine.fgSelected;
                        anchors.horizontalCenter: parent.horizontalCenter;
                    }
                    ColumnContainer {
                        spacing: Engine.spaceS;
                        anchors.horizontalCenter: parent.horizontalCenter;

                        Repeater {
                            model: [
                                { value: ZoomLevel.XSMALL, label: $ (qsTr ("Extra Small")) },
                                { value: ZoomLevel.SMALL,  label: $ (qsTr ("Small")) },
                                { value: ZoomLevel.MEDIUM, label: $ (qsTr ("Medium")) },
                                { value: ZoomLevel.LARGE,  label: $ (qsTr ("Large")) },
                                { value: ZoomLevel.XLARGE, label: $ (qsTr ("Extra Large")) },
                            ];
                            delegate: TextButton {
                                label: modelData ["label"];
                                checked: (Engine.currentLevel === modelData ["value"]);
                                textSize: {
                                    switch (modelData ["value"]) {
                                       case ZoomLevel.XSMALL: return Engine.previewTextSizeXS;
                                       case ZoomLevel.SMALL:  return Engine.previewTextSizeS;
                                       case ZoomLevel.LARGE:  return Engine.previewTextSizeL;
                                       case ZoomLevel.XLARGE: return Engine.previewTextSizeXL;
                                    }
                                    return Engine.previewTextSizeM;
                                }
                                ExtraAnchors.horizontalFill: parent;
                                onClicked: {
                                    Engine.currentLevel = modelData ["value"];
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
