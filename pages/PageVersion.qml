import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;
import "../components";

LazyPage {
    id: pageVersion;
    needed: (Engine.currentPage === PageIds.VERSIONS);

    ColumnContainer {
        anchors.fill: parent;

        ToolBar {
            SvgIconLoader {
                icon: "qrc:///img/open.svg";
                color: Engine.fgSelected;
                anchors.verticalCenter: parent.verticalCenter;
            }
            TextLabel {
                text: $ (qsTr ("Version"));
                font.pixelSize: Engine.fontL;
                anchors.verticalCenter: parent.verticalCenter;
            }
            Stretcher { }
            TextButton {
                icon: "qrc:///img/home.svg";
                text: $ (qsTr ("Back to view"));
                anchors.verticalCenter: parent.verticalCenter;
                onClicked: {
                    Engine.currentPage = PageIds.VIEW;
                }
            }
        }
        Item {
            Container.forcedHeight: Engine.spaceM;
        }
        RowContainer {
            spacing: Engine.spaceM;
            anchors.margins: Engine.spaceM;
            ExtraAnchors.horizontalFill: parent;

            TextButton {
                icon: "qrc:///img/refresh.svg";
                text: $ (qsTr ("Refresh index"));
                onClicked: {
                    Engine.refreshIndex ();
                }
            }
            Stretcher { }
            TextButton {
                icon: "qrc:///img/filter.svg";
                text: $ (qsTr ("Show only local"));
                checked: Engine.showLocalOnly;
                invertLayout: true;
                onClicked: {
                    Engine.showLocalOnly = !Engine.showLocalOnly;
                }
            }
        }
        Item {
            Container.forcedHeight: Engine.spaceM;
        }
        Line {
            ExtraAnchors.horizontalFill: parent;
        }
        ScrollContainer {
            id: _scroller;
            placeholder: $ (_scroller.contentHeight === 0 && Engine.refreshErrorMsg === ""
                            ? (Engine.showLocalOnly
                               ? qsTr ("No local Bible,\nplease disable filter\nand download at least one.")
                               : qsTr ("No index of Bibles,\nplease refresh index."))
                            : "");
            ExtraAnchors.horizontalFill: parent;
            Container.verticalStretch: 1;

            Repeater {
                model: Engine.languagesModel;
                delegate: ColumnContainer {
                    id: _lang;
                    visible: (_lang.qtObject.nbLocals || !Engine.showLocalOnly);
                    ExtraAnchors.horizontalFill: parent;

                    readonly property BibleLanguage qtObject: (modelData || null);

                    SwipeDetector {
                        id: _clicker;
                        selected: _lblLang.emphasis;
                        implicitHeight: (_header.height + _header.anchors.margins * 2);
                        onClicked: {
                            _lang.qtObject.expanded = !_lang.qtObject.expanded;
                        }

                        RowContainer {
                            id: _header;
                            spacing: Engine.spaceM;
                            anchors {
                                margins: Engine.spaceM;
                                verticalCenter: parent.verticalCenter;
                            }
                            ExtraAnchors.horizontalFill: parent;

                            SvgIconLoader {
                                icon: (_lang.qtObject.expanded ? "qrc:///img/folder-opened.svg" : "qrc:///img/folder-closed.svg");
                                anchors.verticalCenter: parent.verticalCenter;
                            }
                            TextLabel {
                                id: _lblLang;
                                text: _lang.qtObject.languageTitle.split ("; ").join (" | ");
                                emphasis: (_lang.qtObject.languageId === Engine.currentTextKey.split ("__") [0]);
                                font.pixelSize: Engine.fontL;
                                anchors.verticalCenter: parent.verticalCenter;
                                Container.horizontalStretch: 1;
                            }
                            Item {
                                id: _bubble;
                                visible: _lang.qtObject.nbLocals;
                                implicitWidth: Math.max (_counter.implicitWidth, _counter.implicitHeight);
                                implicitHeight: _counter.implicitHeight;
                                anchors.verticalCenter: parent.verticalCenter;

                                Rectangle {
                                    color: Engine.bgHighlight;
                                    radius: (_bubble.width * 0.5);
                                    opacity: 0.65;
                                    anchors.fill: parent;
                                }
                                TextLabel {
                                    id: _counter;
                                    text: _lang.qtObject.nbLocals;
                                    color: Engine.fgSelected;
                                    padding: Engine.spaceS;
                                    emphasis: true;
                                    anchors.centerIn: parent;
                                }
                            }
                        }
                    }
                    Column {
                        id: layoutTexts;
                        visible: _lang.qtObject.expanded;
                        ExtraAnchors.horizontalFill: parent;

                        Repeater {
                            model: (_lang.visible && _lang.qtObject.expanded ? _lang.qtObject.textsModel : 0);
                            delegate: SwipeDetector {
                                id: _version;
                                visible: (_version.qtObject.hasLocal || !Engine.showLocalOnly);
                                selected: _lblVersion.emphasis;
                                implicitHeight: (_line.height + _line.anchors.margins * 2);
                                onClicked: {
                                    if (_version.qtObject.hasLocal) {
                                        Engine.loadText (_version.qtObject.textKey);
                                        Engine.currentPage = PageIds.VIEW;
                                    }
                                }

                                readonly property BibleText qtObject: (modelData || null);

                                RowContainer {
                                    id: _line;
                                    spacing: Engine.spaceM;
                                    anchors {
                                        margins: Engine.spaceM;
                                        verticalCenter: parent.verticalCenter;
                                    }
                                    ExtraAnchors.horizontalFill: parent;

                                    Stretcher {
                                        Container.forcedWidth: (20 * Engine.zoomRatio);
                                    }
                                    TextLabel {
                                        id: _lblVersion;
                                        text: _version.qtObject.bibleTitle;
                                        emphasis: (_version.qtObject.textKey === Engine.currentTextKey);
                                        anchors.verticalCenter: parent.verticalCenter;
                                        Container.horizontalStretch: 1;
                                    }
                                    TextLabel {
                                        text: "%1 %".arg (_version.qtObject.percent);
                                        visible: _version.qtObject.isLoading;
                                        anchors.verticalCenter: parent.verticalCenter;
                                    }
                                    SvgIconLoader {
                                        icon: "qrc:///img/ok.svg";
                                        color: Engine.fgSuccess;
                                        visible: _version.qtObject.hasLocal;
                                        anchors.verticalCenter: parent.verticalCenter;
                                    }
                                    TextButton {
                                        id: btnDownload;
                                        icon: "qrc:///img/save.svg";
                                        visible: !_version.qtObject.hasLocal;
                                        enabled: !_version.qtObject.isLoading;
                                        anchors.verticalCenter: parent.verticalCenter;
                                        onClicked: {
                                            Engine.downloadText (_lang.qtObject.languageId, _version.qtObject.bibleId);
                                        }
                                    }
                                    TextButton {
                                        id: btnRemove;
                                        icon: "qrc:///img/trash.svg";
                                        enabled: !_lblVersion.emphasis;
                                        visible: _version.qtObject.hasLocal;
                                        anchors.verticalCenter: parent.verticalCenter;
                                        onClicked: {
                                            _confirm.createObject (Window.window, {
                                                                       bibleText: _version.qtObject,
                                                                   });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        ExpandCollapseBar {
            model: Engine.languagesModel;
        }
        Component {
            id: _confirm;

            ConfirmRemoveBible { }
        }
    }
}
