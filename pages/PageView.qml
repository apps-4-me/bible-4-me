import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;
import "../components";

FocusScope {
    id: _self;
    enabled: (Engine.currentPage === PageIds.VIEW);
    onCurrentDelegateChanged: {
        _timer.restart ();
    }

    property Item currentDelegate: null;

    signal contextActionsRequested (BibleVerse verse);

    function clamp (val, min, max) {
        return (val > max ? max : (val < min ? min : val));
    }

    Timer {
        id: _timer;
        repeat: false;
        running: false;
        interval: 350;
        onTriggered: {
            if (_self.currentDelegate) {
                _scroller.contentY = ((_scroller.contentHeight > _scroller.height)
                                      ? _self.clamp ((_self.currentDelegate.y - (_scroller.height / 2) + (_self.currentDelegate.height / 2)),
                                                     0,
                                                     (_scroller.contentHeight - _scroller.height))
                                      : 0);
            }
        }
    }
    ColumnContainer {
        anchors.fill: parent;

        ToolBar {
            TextButton {
                icon: "qrc:///img/open.svg";
                anchors.verticalCenter: parent.verticalCenter;
                onClicked: {
                    Engine.currentPage = PageIds.VERSIONS;
                }
            }
            TextLabel {
                text: "Bible'me";
                font.pixelSize: Engine.fontL;
                horizontalAlignment: Text.AlignHCenter;
                anchors.verticalCenter: parent.verticalCenter;
                Container.horizontalStretch: 1;
            }
            TextButton {
                icon: "qrc:///img/mark.svg";
                anchors.verticalCenter: parent.verticalCenter;
                onClicked: {
                    Engine.currentPage = PageIds.FAVORITES;
                }
            }
            TextButton {
                icon: "qrc:///img/find.svg";
                anchors.verticalCenter: parent.verticalCenter;
                onClicked: {
                    Engine.currentPage = PageIds.SEARCH;
                }
            }
            TextButton {
                icon: "qrc:///img/config.svg";
                anchors.verticalCenter: parent.verticalCenter;
                onClicked: {
                    Engine.currentPage = PageIds.CONFIG;
                }
            }
        }
        Item {
            Container.forcedHeight: Engine.spaceM;
        }
        RowContainer {
            anchors.margins: Engine.spaceM;
            ExtraAnchors.horizontalFill: parent;

            TextLabel {
                text: $ (Engine.formatReference (Engine.currentChapterId));
                opacity: 0.65;
                anchors.verticalCenter: parent.verticalCenter;
            }
            Stretcher { }
            TextLabel {
                text: $ ("(%1)".arg (Engine.currentText ? Engine.currentText.bibleTitle : qsTr ("no version")));
                opacity: 0.35;
                font.pixelSize: Engine.fontS;
                anchors.verticalCenter: parent.verticalCenter;
            }
        }
        Item {
            Container.forcedHeight: Engine.spaceM;
        }
        Line {
            ExtraAnchors.horizontalFill: parent;
        }
        ScrollContainer {
            id: _scroller;
            placeholder: $ ((!Engine.currentChapter || !Engine.currentChapter.verseModel.length)
                            ? (!Engine.currentTextKey
                               ? qsTr ("No translation selected,\nplease go and add one.")
                               : (!Engine.currentVerseId
                                  ? qsTr ("No verse selected,\nplease choose a target.")
                                  : qsTr ("This book and/or chapter\ndoesn't exist in this translation.")))
                            : "");
            ExtraAnchors.horizontalFill: parent;
            Container.verticalStretch: 1;

            Repeater {
                id: _repeater;
                model: (Engine.currentChapter ? Engine.currentChapter.verseModel : 0);
                delegate: SwipeDetector {
                    id: _verse;
                    selected: (_verse.qtObject === Engine.currentVerse);
                    implicitHeight: (_line.implicitHeight + _line.anchors.margins * 2);
                    onClicked: {
                        Engine.selectPosition (_verse.qtObject.verseId);
                    }
                    onPressAndHold: {
                        Engine.selectPosition (_verse.qtObject.verseId);
                        _self.contextActionsRequested (_verse.qtObject);
                    }
                    onIsCurrentChanged: {
                        if (_verse.isCurrent) {
                            _self.currentDelegate = _verse;
                        }
                    }
                    Component.onCompleted: {
                        if (_verse.isCurrent) {
                            _self.currentDelegate = _verse;
                        }
                    }

                    readonly property BibleVerse qtObject: (modelData || null);
                    readonly property bool isCurrent: (Engine.currentVerse && _verse.qtObject && _verse.qtObject === Engine.currentVerse);

                    RowContainer {
                        id: _line;
                        spacing: Engine.spaceS;
                        anchors {
                            margins: Engine.spaceS;
                            verticalCenter: parent.verticalCenter;
                        }
                        ExtraAnchors.horizontalFill: parent;

                        TextLabel {
                            text: "%1.".arg (model.index +1);
                            color: Engine.fgSelected;
                            emphasis: _verse.qtObject.marked;
                            font.pixelSize: Engine.currentTextSize;
                        }
                        TextLabel {
                            text: _verse.qtObject.textContent;
                            wrapMode: Text.WrapAtWordBoundaryOrAnywhere;
                            emphasis: _verse.qtObject.marked;
                            font.pixelSize: Engine.currentTextSize;
                            Container.horizontalStretch: 1;
                        }
                    }
                }
            }
        }
        StatusBar {
            TextButton {
                id: _btnPrev;
                icon: "qrc:///img/chevron-left.svg";
                text: $ (qsTr ("Previous chapter"));
                enabled: (Engine.currentBook && Engine.currentChapterNum > 1);
                anchors.verticalCenter: parent.verticalCenter;
                onClicked: {
                    _animPrev.start ();
                    Engine.selectPosition ("%1.%2.1".arg (Engine.currentBookId).arg (Engine.currentChapterNum -1));
                }
            }
            Stretcher { }
            TextButton {
                icon: "qrc:///img/target.svg";
                anchors.verticalCenter: parent.verticalCenter;
                onClicked: {
                    Engine.currentPage = PageIds.SELECTOR;
                }
            }
            Stretcher { }
            TextButton {
                id: _btnNext;
                icon: "qrc:///img/chevron-right.svg";
                text: $ (qsTr ("Next chapter"));
                enabled: (Engine.currentBook && Engine.currentChapterNum < Engine.currentBook.chaptersModel.length);
                invertLayout: true;
                anchors.verticalCenter: parent.verticalCenter;
                onClicked: {
                    _animNext.start ();
                    Engine.selectPosition ("%1.%2.1".arg (Engine.currentBookId).arg (Engine.currentChapterNum +1));
                }
            }
        }
    }
    SvgIconLoader {
        icon: "qrc:///img/chevron-left.svg";
        size: (Math.min (parent.width, parent.height) * 0.35);
        color: Engine.fgText;
        opacity: 0.0;
        anchors.centerIn: parent;

        NumberAnimation on opacity {
            id: _animPrev;
            loops: 1;
            running: false;
            alwaysRunToEnd: true;
            from: 1.0;
            to: 0.0;
            duration: 650;
        }
        Rectangle {
            z: -1;
            color: Engine.fgText;
            radius: (width * 0.5);
            antialiasing: true;
            opacity: 0.35;
            anchors {
                fill: parent;
                margins: (parent.size * -0.15);
            }
        }
    }
    SvgIconLoader {
        icon: "qrc:///img/chevron-right.svg";
        size: (Math.min (parent.width, parent.height) * 0.35);
        color: Engine.fgText;
        opacity: 0.0;
        anchors.centerIn: parent;

        NumberAnimation on opacity {
            id: _animNext;
            loops: 1;
            running: false;
            alwaysRunToEnd: true;
            from: 1.0;
            to: 0.0;
            duration: 650;
        }
        Rectangle {
            z: -1;
            color: Engine.fgText;
            radius: (width * 0.5);
            antialiasing: true;
            opacity: 0.35;
            anchors {
                fill: parent;
                margins: (parent.size * -0.15);
            }
        }
    }
}
