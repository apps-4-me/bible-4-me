import QtQuick 2.15;
import QtQuick.Window 2.15;
import BibleMe 1.0;
import "components";
import "pages";

Window {
    id: _window;
    color: Engine.bgPrimary;
    title: $ (qsTr ("Bible'me"));
    width: 480;
    height: 800;
    visible: true;
    visibility: (Qt.platform.os === "android" || Qt.platform.os === "ios" ? Window.FullScreen : Window.Windowed);

    function $ (str) {
        return (Engine.currentTranslationCode !== "" ? str : str + "");
    }

    function showContextActions (verse) {
        _compoCtx.createObject (_window, {
                                    bibleVerse: verse,
                                });
    }

    FontLoader {
        id: _fontloader;
        source: "qrc:///fonts/Ubuntu-R.ttf";
    }
    Binding {
        target: Engine;
        property: "fontName";
        value: _fontloader.name;
    }
    FocusScope {
        id: workspace;
        focus: true;
        anchors {
            fill: parent;
            bottomMargin: ((Qt.inputMethod.visible && Qt.inputMethod.keyboardRectangle.y < Window.window.height)
                           ? (Window.window.height - Qt.inputMethod.keyboardRectangle.y)
                           : 0);
        }
        Keys.onBackPressed: {
            Engine.currentPage = PageIds.VIEW;
        }

        Row {
            id: layoutPages;
            x: (_window.width * factor);
            ExtraAnchors.verticalFill: parent;

            property real factor: {
                switch (Engine.currentPage) {
                case PageIds.VIEW:
                    return -1;
                case PageIds.FAVORITES:
                case PageIds.SEARCH:
                case PageIds.CONFIG:
                    return -2;
                case PageIds.VERSIONS:
                case PageIds.SELECTOR:
                    return 0;
                }
                return -9;
            }

            Behavior on factor { NumberAnimation { duration: 150; } }
            Item {
                implicitWidth: _window.width;
                ExtraAnchors.verticalFill: parent;

                PageVersion { }
                PageSelect { }
            }
            PageView {
                implicitWidth: _window.width;
                ExtraAnchors.verticalFill: parent;
                onContextActionsRequested: {
                    _window.showContextActions (verse);
                }
            }
            Item {
                implicitWidth: _window.width;
                ExtraAnchors.verticalFill: parent;

                PageSearch {
                    onContextActionsRequested: {
                        _window.showContextActions (verse);
                    }
                }
                PageFavorites {
                    onContextActionsRequested: {
                        _window.showContextActions (verse);
                    }
                }
                PageSettings { }
            }
        }
        MouseArea {
            id: throbber;
            anchors.fill: workspace;
            states: [
                State {
                    name: "idle";
                    when: (!Engine.isRefreshing && !Engine.isSearching);

                    PropertyChanges {
                        target: throbber;
                        visible: false;
                    }
                },
                State {
                    name: "refreshing";
                    when: Engine.isRefreshing;

                    PropertyChanges {
                        target: throbber;
                        visible: true;
                    }
                    PropertyChanges {
                        target: lblOperationTitle;
                        text: $ (qsTr ("Refreshing index..."));
                    }
                    PropertyChanges {
                        target: lblOperationPercent;
                        text: "%1 %".arg (Engine.refreshPercent);
                    }
                },
                State {
                    name: "searching";
                    when: Engine.isSearching;

                    PropertyChanges {
                        target: throbber;
                        visible: true;
                    }
                    PropertyChanges {
                        target: lblOperationTitle;
                        text: $ (qsTr ("Searching..."));
                    }
                    PropertyChanges {
                        target: lblOperationPercent;
                        text: "%1 %".arg (Engine.searchPercent);
                    }
                }
            ]
            onWheel:    { }
            onPressed:  { }
            onReleased: { }

            Rectangle {
                id: dimmer;
                color: Engine.fgText;
                opacity: 0.85;
                anchors.fill: parent;
            }
            Column {
                spacing: Engine.spaceL;
                anchors.centerIn: parent;

                TextLabel {
                    id: lblOperationTitle;
                    color: Engine.bgPrimary;
                    font.pixelSize: Engine.fontL;
                    anchors.horizontalCenter: parent.horizontalCenter;
                }
                Stretcher {
                    width: icoThrobber.size;
                    height: icoThrobber.size;
                    anchors.horizontalCenter: parent.horizontalCenter;

                    SvgIconLoader {
                        id: icoThrobber;
                        icon: "qrc:///img/refresh";
                        size: (_window.width * 0.35);
                        color: Engine.bgPrimary;
                        anchors.centerIn: parent;

                        NumberAnimation on rotation {
                            loops: Animation.Infinite;
                            running: throbber.visible;
                            alwaysRunToEnd: true;
                            from: 359;
                            to: 0;
                            duration: 850;
                        }
                    }
                    TextLabel {
                        id: lblOperationPercent;
                        color: Engine.bgPrimary;
                        font.pixelSize: Engine.fontL;
                        anchors.centerIn: parent;
                    }
                }
            }
        }
    }
    Component {
        id: _compoCtx;

        DialogVerse { }
    }
}
